#!/usr/bin/env sh

find . \( -name '*.fcstd1' -o -name '*.pyc' -o -name '*~' \) -exec rm -f {} \;
