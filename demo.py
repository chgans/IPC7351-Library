
import sys
sys.path.append("/usr/lib/freecad/lib")

import FreeCAD as App
import FreeCADGui as Gui
import Part
import math
import os

try:
    reload(Distributor)
except:
    import Distributor
try:
    reload(IpcPackageBuilder)
except:
    import IpcPackageBuilder

# mw=Gui.getMainWindow()
# mw.hide()

###################################################################################3
# TODO: move to os utils

def rmFile(filename):
    try:
        os.remove(filename)
    except OSError as exc:
        if exc.errno != errno.ENOENT:
            raise exc
        pass

def mkdir_p(path):
    import errno
    try:
       os.mkdir(path)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise exc
        pass


def makePackage(builder):
    outputPath = "out"
    mkdir_p(outputPath)
    doc = builder.build()
    stepName = builder.ipcName + ".step"
    #fcName = builder.ipcName + ".fcstd"
    doc.recompute()
    import Import
    # TODO: doc.getObjectsByLabel(ipcName)
    Import.export(doc.getObjectsByLabel("package"), os.path.join(outputPath, stepName))

def sol():
    builder = IpcPackageBuilder.SolBuilder()
    builder.A = 1.10
    builder.b = 0.225
    builder.D = 2.00
    builder.E = 2.20
    builder.E1 = 1.70
    builder.E2 = 1.65
    builder.e = 0.5
    builder.Dn = 4
    makePackage(builder)

def soj():
    builder = IpcPackageBuilder.SojBuilder()
    builder.A = 3.56
    builder.b = 0.435
    builder.D = 12.80
    builder.E = 8.66
    builder.E1 = 7.52
    builder.E2 = 6.80
    builder.e = 1.27
    builder.Dn = 10
    makePackage(builder)

def sow():
    builder = IpcPackageBuilder.SowBuilder()
    builder.A = 1.2
    builder.A1 = 0.05
    builder.b = 0.245
    builder.D = 7.80
    builder.E = 6.40
    builder.E1 = 4.40
    builder.L = 0.6
    builder.e = 0.65
    builder.Dn = 12
    makePackage(builder)

def son():
    builder = IpcPackageBuilder.SonBuilder()
    builder.A = 0.8
    builder.b = 0.25
    builder.D = 4.00
    builder.E = 3.00
    builder.L = 0.4
    builder.L1 = 0.6
    builder.e = 0.50
    builder.Dn = 7
    makePackage(builder)

def pson():
    builder = IpcPackageBuilder.PsonBuilder()
    builder.A = 0.8
    builder.b = 0.25
    builder.D = 4.00
    builder.E = 4.00
    builder.L = 0.50
    builder.L1 = 0.70
    builder.L2 = 0.10
    builder.e = 0.50
    builder.Dn = 7
    makePackage(builder)

def plcc():
    builder = IpcPackageBuilder.PlccBuilder()
    builder.A = 4.57
    builder.D = 12.445
    builder.D1 = 11.50
    builder.D2 = 10.60
    builder.E = 12.445
    builder.E1 = 11.50
    builder.e = 1.27
    builder.b = 0.43
    builder.Dn = 7
    builder.En = 7
    makePackage(builder)

def qfp():
    builder = IpcPackageBuilder.QfpBuilder()
    builder.A = 1.20
    builder.A1 = 0.05
    builder.D = 12.90
    builder.D1 = 10.00
    builder.E = 12.9
    builder.E1 = 10.00
    builder.L = 0.905
    builder.e = 0.50
    builder.b = 0.22
    builder.Dn = 16
    builder.En = 16
    makePackage(builder)

def cqfp():
    from IpcPackageBuilder import QfpBuilder
    builder = QfpBuilder()
    builder.A = 4.92
    builder.A1 = 0.51
    builder.D = 22.35
    builder.D1 = 17.28
    builder.E = 22.35
    builder.E1 = 17.28
    builder.L = 2.0
    builder.e = 1.27
    builder.b = 0.265
    builder.Dn = 13
    builder.En = 13
    makePackage(builder)

def qfn():
    from IpcPackageBuilder import QfnBuilder
    builder = QfnBuilder()
    builder.A = 0.8
    builder.D = 5.0
    builder.E = 4.0
    builder.L = 0.4
    builder.e = 0.5
    builder.b = 0.25
    builder.Dn = 8
    builder.En = 6
    makePackage(builder)

def pqfn():
    builder = IpcPackageBuilder.PqfnBuilder()
    builder.A = 0.8
    builder.D = 5.0
    builder.E = 4.0
    builder.L = 0.4
    builder.L1 = 0.10
    builder.e = 0.5
    builder.b = 0.25
    builder.Dn = 7
    builder.En = 5
    makePackage(builder)

def chip():
    builder = IpcPackageBuilder.ChipBuilder()
    builder.A = 1.40
    builder.D = 3.20
    builder.E = 1.60
    builder.L = 0.50
    builder.L1 = 0.60
    makePackage(builder)

def molded():
    builder = IpcPackageBuilder.MoldedBuilder()
    builder.A = 3.10
    builder.b = 2.40
    builder.D = 7.30
    builder.E = 4.30
    builder.L = 1.30
    builder.L1 = 1.50
    makePackage(builder)

def melf():
    builder = IpcPackageBuilder.MelfBuilder()
    builder.D = 3.30
    builder.E = 1.45
    builder.L = 0.35
    makePackage(builder)

def sod():
    builder = IpcPackageBuilder.SodBuilder()
    builder.A = 1.27
    builder.A1 = 0.01
    builder.b = 0.65
    builder.D = 3.75
    builder.D1 = 2.70
    builder.E = 1.60
    builder.L = 0.285
    makePackage(builder)

def sodfl():
    builder = IpcPackageBuilder.SodflBuilder()
    builder.A = 0.70
    builder.b = 0.3
    builder.D = 1.60
    builder.D1 = 1.2
    builder.E = 0.8
    builder.L = 0.2
    makePackage(builder)

def dfn2():
    builder = IpcPackageBuilder.Dfn2Builder()
    builder.A = 0.50
    builder.b = 0.505
    builder.D = 1.00
    builder.E = 0.60
    builder.e = 0.65
    builder.L = 0.25
    makePackage(builder)

def dfn4():
    builder = IpcPackageBuilder.Dfn4Builder()
    builder.A = 0.40
    builder.b = 0.255
    builder.D = 1.20
    builder.d = 0.75
    builder.E = 0.775
    builder.e = 0.45
    builder.L = 0.35
    makePackage(builder)

def sc4():
    builder = IpcPackageBuilder.SideConcaveBuilder()
    builder.A = 0.80
    builder.b = 0.70
    builder.D = 2.05
    builder.Dn = 2
    builder.E = 1.60
    builder.e = 0.95
    builder.L = 0.30
    makePackage(builder)

def cav2():
    builder = IpcPackageBuilder.CavBuilder()
    builder.A = 0.70
    builder.b = 0.80
    builder.D = 5.08
    builder.Dn = 4
    builder.E = 2.10
    builder.En = 0
    builder.e = 1.27
    builder.L = 0.40
    makePackage(builder)

def cav4():
    builder = IpcPackageBuilder.CavBuilder()
    builder.A = 0.65
    builder.b = 0.40
    builder.b1 = 0.50
    builder.D = 4.0
    builder.Dn = 4
    builder.E = 2.10
    builder.En = 1
    builder.e = 0.8
    builder.L = 0.40
    builder.L1 = 0.45
    makePackage(builder)

def main():
    window = None
    if not App.GuiUp:
        Gui.showMainWindow()
        window = Gui.getMainWindow()
        window.hide()
    sol()
    soj()
#    sow()
#    son()
#    pson()
#    plcc()
#    qfp()
#    cqfp()
#    qfn()
#    pqfn()
#    chip()
#    molded()
#    melf()
#    sod()
#    sodfl()
#    dfn2()
#    dfn4()
    sc4()
    if window is not None:
        from PySide import QtGui
        window.show()
        QtGui.QApplication.instance().exec_()


if __name__ == '__main__':
    main()
