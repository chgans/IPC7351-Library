- FC doc meta data (license, author)
- material and colors
- STEP export: export the shape and it's placement (package origin)
- when savind <package>.fcstd, make sure it contains copy of original data
- auto unzip fcstd in git
- auto unzip ods in git
- move each lead into their own doc, or don't share params as one might become
  invalid because of valid value are targeted for another

when using the symetry constraint to create a mid-point and then set a vertical constraint on it:
Sketcher::Solve()-DogLeg- Failed!! Falling back...
Important: the LevenbergMarquardt solver succeeded where the DogLeg solver had failed.
If you see this message please report a way of reproducing this result at
http://www.freecadweb.org/tracker/main_page.php
