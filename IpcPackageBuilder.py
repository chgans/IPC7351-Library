# -*- coding: utf-8 -*-

# Python
import math, os

# Qt
from PySide import QtGui, QtCore

# FreeCAD
import FreeCAD as App

# Local
import Distributor

###########################################################################
# <TODO what="Get rid of this">
###########################################################################

def rmFile(filename):
    try:
        os.remove(filename)
    except OSError:
        pass

def closeDocument(name):
    try:
        App.closeDocument(name)
    except:
        pass

def newDocument(name):
    fcName = name + ".fcstd"
    closeDocument(name)
    rmFile(fcName)
    return App.newDocument(name)

def getOrOpenDocument(name, filename):
    try:
        return App.getDocument(name)
    except:
        return App.openDocument(filename)

# , PartGui, math
# from FreeCAD import Base

class Item:
    def __init__(self, shape = None, placement = None):
        self.shape = shape
        self.placement = placement

    def addToDocument(self, doc, name, transform):
        obj = doc.addObject("Part::Feature", name)
        obj.Label = name
        obj.Shape = self.shape
        obj.Placement = transform * self.placement.toMatrix()
        return obj

class Package:
    def __init__(self, body = None, terminals = []):
        self.body = body
        self.terminals = terminals
        self.name = "NONAME"

    def addToDocument(self, doc, name, transform = App.Matrix()):
        # Faster, keep colors,
        # obj = doc.addObject("Part::Compound", name)
        # obj.Links = [self.body] + self.terminals

        # Slower, doesn't keep colors,
        obj = doc.addObject("Part::MultiFuse", name)
        obj.Shapes = [self.body] + self.terminals

        obj.Label = name
        obj.Placement = transform
        return obj


def ipcName(fmt, values):
    numbers = ()
    for value in values:
        if type(value) == type(0.0):
            numbers = numbers + (str(int(round(value*100))),)
        else:
            numbers = numbers + (str(value),)
    return fmt % (numbers)

def getObject(doc, typeId, label):
    for object in doc.findObjects(typeId):
        if object.Label == label:
            return object
    return None

def createTerminal(profileName, featureParams):
    doc = getOrOpenDocument(profileName, "terminals/" + profileName + ".fcstd")
    for key, value in featureParams.iteritems():
        doc.params.set(key, value)

    doc.recompute()

    feature = getObject(doc, "Part::Feature", "terminal")
    item = Item(feature.Shape, feature.Placement)

    App.closeDocument(profileName)
    return item


def createBody(name, params):
    doc = None
    try:
        doc = App.getDocument(name)
    except:
        doc = App.openDocument("bodies/" + name + ".fcstd")

    for key, value in params.iteritems():
        doc.params.set(key, value)

    feature = getObject(doc, "Part::Feature", "body")
    feature.touch() # necessary due other sketch getting invalid
    doc.recompute()

    item = Item(feature.Shape, feature.Placement)
    App.closeDocument(name)
    return item

###########################################################################
# <TODO/>
###########################################################################

class AbstractBuilder:
    def __init__(self):
        self.doc = None
        self.body = None
        self.terminals = []
        self.cutouts = []

    def _buildAll(self):
        base = self.doc.addObject("Part::MultiFuse")
        base.Shapes = [self.body] + self.terminals
        tool = self.doc.addObject("Part::MultiFuse")
        tool.Shapes = self.cutouts
        obj = self.doc.addObject("Part::Cut")
        obj.Base = base
        obj.Tool = tool
        obj.Label = "package"
        return obj

    def _makeObject(self, filePath, docParams, objName):
        doc = App.openDocument(filePath)
        for key, value in docParams.iteritems():
            doc.params.set(key, str(value))
        doc.recompute()
        obj = doc.getObjectsByLabel(objName)[0]
        NO_DEP = False
        obj = self.doc.copyObject(obj, NO_DEP)
        App.closeDocument(doc.Name)
        return obj



class DilPackageBuilder:
    def __init__(self):
        self.IpcName = None
        self.bodyStyle = None
        self.bodyParams = None
        self.bodyTransform = App.Matrix()
        self.bodyColor = (64/255.0, 64/255.0, 64/255.0)
        self.terminalStyle = None
        self.terminalParams = None
        self.terminalSpan = None
        self.terminalPitch = None
        self.terminalCount = None
        self.terminalTransform = App.Matrix()
        self.terminalColor = (192/255.0, 192/255.0, 192/255.0)

    def build(self):
        dist = Distributor.Distributor(0, 0, 0,
                                       self.terminalCount, self.terminalPitch, self.terminalSpan)
        self.terminalTransforms = dist.distribution()
        self.pinCount = self.terminalCount*2
        doc = newDocument(self.ipcName)
        bodyItem = createBody(self.bodyStyle, self.bodyParams)
        body = bodyItem.addToDocument(doc, "Body", self.bodyTransform)
        body.ViewObject.ShapeColor = self.bodyColor
        leads = []
        terminalItem = createTerminal(self.terminalStyle, self.terminalParams)
        bot, right, top, left = self.terminalTransforms
        i = 1
        for transform in bot:
            lead = terminalItem.addToDocument(doc, "Term" + str(i), transform)
            lead.ViewObject.ShapeColor = self.terminalColor
            leads.append(lead)
            i = i + 1
        for transform in top:
            lead = terminalItem.addToDocument(doc, "Term" + str(i), transform)
            lead.ViewObject.ShapeColor = self.terminalColor
            leads.append(lead)
        packageItem = Package(body, leads)
        package = packageItem.addToDocument(doc, "package")
        return doc

class QilPackageBuilder():
    def __init__(self):
        self.IpcName = None
        self.bodyStyle = None
        self.bodyParams = None
        self.bodyTransform = App.Matrix()
        self.bodyColor = (64/255.0, 64/255.0, 64/255.0)
        self.terminalStyle = None
        self.terminalPitch = None
        self.terminalTransform = App.Matrix()
        self.terminalColor = (192/255.0, 192/255.0, 192/255.0)
        self.rowTerminalParams = None
        self.rowTerminalSpan = None
        self.rowTerminalCount = 0
        self.colTerminalParams = None
        self.colTerminalSpan = None
        self.colTerminalCount = 0

    def build(self):
        dist = Distributor.Distributor(self.rowTerminalCount, self.terminalPitch, self.rowTerminalSpan,
                                       self.colTerminalCount, self.terminalPitch, self.colTerminalSpan)
        self.terminalTransforms = dist.distribution()
        self.pinCount = self.rowTerminalCount*2 + self.colTerminalCount*2
        doc = newDocument(self.ipcName)
        bodyItem = createBody(self.bodyStyle, self.bodyParams)
        body = bodyItem.addToDocument(doc, "Body", self.bodyTransform)
        body.ViewObject.ShapeColor = self.bodyColor
        leads = []
        rowTerminalItem = createTerminal(self.terminalStyle, self.rowTerminalParams)
        colTerminalItem = createTerminal(self.terminalStyle, self.colTerminalParams)
        bot, right, top, left = self.terminalTransforms
        i = 1
        for transform in bot:
            lead = colTerminalItem.addToDocument(doc, "Term" + str(i), transform*self.terminalTransform)
            lead.ViewObject.ShapeColor = self.terminalColor
            leads.append(lead)
            i = i + 1
        for transform in right:
            lead = rowTerminalItem.addToDocument(doc, "Term" + str(i), transform*self.terminalTransform)
            lead.ViewObject.ShapeColor = self.terminalColor
            leads.append(lead)
            i = i + 1
        for transform in top:
            lead = colTerminalItem.addToDocument(doc, "Term" + str(i), transform*self.terminalTransform)
            lead.ViewObject.ShapeColor = self.terminalColor
            leads.append(lead)
            i = i + 1
        for transform in left:
            lead = rowTerminalItem.addToDocument(doc, "Term" + str(i), transform*self.terminalTransform)
            lead.ViewObject.ShapeColor = self.terminalColor
            leads.append(lead)
            i = i + 1
        packageItem = Package(body, leads)
        package = packageItem.addToDocument(doc, "package")
        return doc

# SOL and OSCL
class SolBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.D = None # min-max
        self.E = None # min-max
        self.E1 = None # min-max
        self.E2 = None # nom
        self.e = None # ref
        self.b = None # min-max
        self.Dn = None
        # Constants
        self.leadThickness = 0.2
        # Output
        self.ipcName = None
        self.pinCount = None

    def build(self):
        self.ipcName = "SOL_FIXME"
        self.A1 = self.leadThickness
        builder = DilPackageBuilder()
        builder.ipcName = self.ipcName
        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D),
            "B1": str(self.E1),
            "C1": str(self.A-self.A1)
        }
        builder.bodyTransform = App.Matrix(1, 0, 0, 0,
                                           0, 1, 0, 0,
                                           0, 0, 1, self.A1,
                                           0, 0, 0, 1)
        builder.terminalStyle = "inward_l_lead"
        builder.terminalParams = {
            "A1": str((self.E-self.E1)/2.0),
            "A2": str(self.E-self.E2),
            "B1": str(self.b),
            "C1": str((self.A)/2.0+self.leadThickness),
            "C2": str(self.leadThickness),
        }
        builder.terminalSpan = self.E
        builder.terminalPitch = self.e
        builder.terminalCount = self.Dn
        builder.terminalTransform = App.Matrix()
        return builder.build()

# SOJ and OSCJ
class SojBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.D = None # min-max
        self.E = None # min-max
        self.E1 = None # min-max
        self.E2 = None # nom
        self.e = None # ref
        self.b = None # min-max
        self.Dn = None
        # Constants
        self.leadThickness = 0.2
        # Output
        self.ipcName = None
        self.pinCount = None

    def build(self):
        self.L = self.E - self.E2
        self.A1 = self.L/2.0
        self.ipcName = "SOJ_FIXME"
        builder = DilPackageBuilder()
        builder.ipcName = self.ipcName
        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D),
            "B1": str(self.E1),
            "C1": str(self.A-self.A1)
        }
        builder.bodyTransform = App.Matrix(1, 0, 0, 0,
                                           0, 1, 0, 0,
                                           0, 0, 1, self.A1,
                                           0, 0, 0, 1)
        builder.terminalStyle = "j_lead"
        builder.terminalParams = {
            "A1": str((self.E-self.E1)/2.0),
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.A1 + (self.A-self.A1)/2.0 + self.leadThickness/2.0),
            "C2": str(self.leadThickness),
        }
        builder.terminalSpan = self.E
        builder.terminalPitch = self.e
        builder.terminalCount = self.Dn
        builder.terminalTransform = App.Matrix()
        return builder.build()


# TODO: Same lead style and calculation for SOIC, SOP and CFP
# But different IPC name and potentially different body shape
class SowBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.A1 = None # min
        self.D = None # min-max
        self.E = None # min-max
        self.E1 = None # min-max
        self.L = None # min-max
        self.e = None # ref
        self.b = None # min-max
        self.Dn = None
        # Constants
        self.leadThickness = 0.2
        # Output
        self.ipcName = None
        self.pinCount = None

    def build(self):
        self.ipcName = "SOW_FIXME"
        builder = DilPackageBuilder()
        builder.ipcName = self.ipcName
        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D),
            "B1": str(self.E1),
            "C1": str(self.A-self.A1)
        }
        builder.bodyTransform = App.Matrix(1, 0, 0, 0,
                                           0, 1, 0, 0,
                                           0, 0, 1, self.A1,
                                           0, 0, 0, 1)
        builder.terminalStyle = "gull_wing_lead"
        builder.terminalParams = {
            "A1": str((self.E-self.E1)/2.0),
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.A1 + (self.A-self.A1)/2.0 + self.leadThickness/2.0),
            "C2": str(self.leadThickness),
        }
        builder.terminalSpan = self.E
        builder.terminalPitch = self.e
        builder.terminalCount = self.Dn
        builder.terminalTransform = App.Matrix()
        return builder.build()

# FIXME: use L1 for pin1, L for all others
class SonBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.D = None # min-max
        self.E = None # min-max
        self.L = None # min-max
        self.L1 = None # min-max
        self.e = None # ref
        self.b = None # min-max
        self.Dn = None
        # Constants
        self.leadThickness = 0.2
        # Output
        self.ipcName = None
        self.pinCount = None

    def build(self):
        self.ipcName = "SON_FIXME"
        builder = DilPackageBuilder()
        builder.ipcName = self.ipcName
        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D),
            "B1": str(self.E),
            "C1": str(self.A)
        }
        builder.bodyTransform = App.Matrix()
        builder.terminalStyle = "flat_d_lead"
        builder.terminalParams = {
            "A1": str(0), # FIXME
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.leadThickness),
        }
        builder.terminalSpan = self.E
        builder.terminalPitch = self.e
        builder.terminalCount = self.Dn
        builder.terminalTransform = App.Matrix()
        return builder.build()


# FIXME: Remove A1=L2 from the model, and use TerminalTransfrom for the pull-back distance
# FIXME: use L1 for pin1, L for all others
class PsonBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.D = None # min-max
        self.E = None # min-max
        self.L = None # min-max
        self.L1 = None # min-max
        self.L2 = None # min-max
        self.e = None # ref
        self.b = None # min-max
        self.Dn = None
        # Constants
        self.leadThickness = 0.2
        # Output
        self.ipcName = None
        self.pinCount = None

    def build(self):
        self.ipcName = "PSON_FIXME"
        builder = DilPackageBuilder()
        builder.ipcName = self.ipcName
        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D),
            "B1": str(self.E),
            "C1": str(self.A)
        }
        builder.bodyTransform = App.Matrix()
        builder.terminalStyle = "flat_d_lead"
        builder.terminalParams = {
            "A1": str(-self.L2), # FIXME
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.leadThickness),
        }
        builder.terminalSpan = self.E
        builder.terminalPitch = self.e
        builder.terminalCount = self.Dn
        builder.terminalTransform = App.Matrix()
        return builder.build()


class PlccBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.D = None # min-max
        self.D1 = None # nom
        self.D2 = None # min-max
        self.E = None # min-max
        self.E1 = None # min-max
        self.L = None # min-max
        self.L1 = None # min-max
        self.L2 = None # min-max
        self.e = None # ref
        self.b = None # min-max
        self.Dn = None
        self.En = None
        # Constants
        self.leadThickness = 0.2
        # Output
        self.ipcName = None
        self.pinCount = None

    def build(self):
        self.L = self.D - self.D2
        self.A1 = self.L/2.0
        self.ipcName = "PLCC_FIXME"

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D1),
            "B1": str(self.E1),
            "C1": str(self.A-self.A1)
        }
        builder.bodyTransform = App.Matrix(1, 0, 0, 0,
                                           0, 1, 0, 0,
                                           0, 0, 1, self.A1,
                                           0, 0, 0, 1)
        builder.terminalStyle = "j_lead"
        builder.terminalPitch = self.e
        builder.rowTerminalParams = {
            "A1": str((self.D-self.D1)/2.0),
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.A1 + (self.A-self.A1)/2.0 + self.leadThickness/2.0),
            "C2": str(self.leadThickness),
        }
        builder.rowTerminalSpan = self.D
        builder.rowTerminalCount = self.En

        builder.colTerminalParams = {
            "A1": str((self.E-self.E1)/2.0),
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.A1 + (self.A-self.A1)/2.0 + self.leadThickness/2.0),
            "C2": str(self.leadThickness),
        }
        builder.colTerminalSpan = self.E
        builder.colTerminalCount = self.Dn

        return builder.build()


class QfpBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.A1 = None # min
        self.D = None # min-max
        self.D1 = None # nom
        self.E = None # min-max
        self.E1 = None # min-max
        self.L = None # min-max
        self.e = None # ref
        self.b = None # min-max
        self.Dn = None
        self.En = None
        # Constants
        self.leadThickness = 0.2
        # Output
        self.ipcName = None
        self.pinCount = None

    def build(self):
        self.ipcName = "QFP_FIXME"
        self.pinCount = self.Dn*2 + self.En*2

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D1),
            "B1": str(self.E1),
            "C1": str(self.A-self.A1)
        }
        builder.bodyTransform = App.Matrix(1, 0, 0, 0,
                                           0, 1, 0, 0,
                                           0, 0, 1, self.A1,
                                           0, 0, 0, 1)

        builder.terminalStyle = "gull_wing_lead"
        builder.terminalPitch = self.e
        builder.rowTerminalParams = {
            "A1": str((self.D-self.D1)/2.0),
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.A1 + (self.A-self.A1)/2.0 + self.leadThickness/2.0),
            "C2": str(self.leadThickness),
        }
        builder.rowTerminalSpan = self.D
        builder.rowTerminalCount = self.En
        builder.colTerminalParams = {
            "A1": str((self.E-self.E1)/2.0),
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.A1 + (self.A-self.A1)/2.0 + self.leadThickness/2.0),
            "C2": str(self.leadThickness),
        }
        builder.colTerminalSpan = self.E
        builder.colTerminalCount = self.Dn

        return builder.build()


class QfnBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.D = None # min-max
        self.E = None # min-max
        self.L = None # min-max
        self.e = None # ref
        self.b = None # min-max
        self.Dn = None
        self.En = None
        # Constants
        self.leadThickness = 0.2
        # Output
        self.ipcName = None
        self.pinCount = None

    def build(self):
        self.ipcName = "QFN_FIXME"
        self.pinCount = self.Dn*2 + self.En*2

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D),
            "B1": str(self.E),
            "C1": str(self.A)
        }
        builder.bodyTransform = App.Matrix()

        builder.terminalStyle = "flat_d_lead"
        builder.terminalPitch = self.e
        builder.rowTerminalParams = {
            "A1": str(0), # FIXME
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.leadThickness),
        }
        builder.rowTerminalSpan = self.D
        builder.rowTerminalCount = self.En

        builder.colTerminalParams = {
            "A1": str(0), # FIXME
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.leadThickness),
        }
        builder.colTerminalSpan = self.E
        builder.colTerminalCount = self.Dn

        return builder.build()

# FIXME: same as QFN but with pullback distance
class PqfnBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.D = None # min-max
        self.E = None # min-max
        self.L = None # min-max
        self.L1 = None # min-max
        self.e = None # ref
        self.b = None # min-max
        self.Dn = None
        self.En = None
        # Constants
        self.leadThickness = 0.2
        # Output
        self.ipcName = None
        self.pinCount = None

    def build(self):
        self.ipcName = "PQFN_FIXME"
        self.pinCount = self.Dn*2 + self.En*2

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D),
            "B1": str(self.E),
            "C1": str(self.A)
        }
        builder.bodyTransform = App.Matrix()

        builder.terminalStyle = "flat_d_lead"
        builder.terminalPitch = self.e
        builder.rowTerminalParams = {
            "A1": str(-self.L1), # FIXME
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.leadThickness),
        }
        builder.rowTerminalSpan = self.D
        builder.rowTerminalCount = self.En

        builder.colTerminalParams = {
            "A1": str(-self.L1), # FIXME
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.leadThickness),
        }
        builder.colTerminalSpan = self.E
        builder.colTerminalCount = self.Dn

        return builder.build()

###################################################################################3
# Grid Array
###################################################################################3
# Terminals: collapsing_ball, non_collapsing_ball, column, pillar, circular_land, rectangular_land
# single vs dual pitch, aligned vs staggered distribution

###################################################################################3
# 2 leads
###################################################################################3

# FIXME: L != L1
class ChipBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.D = None # min-max
        self.E = None # min-max
        self.L = None # min-max
        self.L1 = None # min-max, optional, default to L

    def build(self):
        self.ipcName = "CHIP_FIXME"

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D-2*self.L),
            "B1": str(self.E),
            "C1": str(self.A)
        }
        builder.bodyTransform = App.Matrix()

        builder.terminalStyle = "rectangular_end_cap"
        builder.rowTerminalParams = {
            "A1": str(self.L),
            "B1": str(self.E),
            "C1": str(self.A),
        }
        builder.rowTerminalCount = 1
        builder.rowTerminalSpan = self.D

        builder.colTerminalParams = {}
        builder.colTerminalCount = 0
        builder.colTerminalSpan = 0.0

        builder.terminalPitch = 0.0
        builder.terminalTransform = App.Matrix()
        return builder.build()


# FIXME: L != L1, b != b1
class MoldedBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.b = None
        self.b1 = None # min-max, optional, default to b
        self.D = None # min-max
        self.E = None # min-max
        self.L = None # min-max
        self.L1 = None # min-max, optional, default to L
        # constants
        self.leadThickness = 0.2

    def build(self):
        self.ipcName = "MOLDED_FIXME"

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D - 2*1.5*self.leadThickness),
            "B1": str(self.E),
            "C1": str(self.A - self.leadThickness)
        }
        builder.bodyTransform = App.Matrix(1, 0, 0, 0,
                                           0, 1, 0, 0,
                                           0, 0, 1, self.leadThickness)

        builder.terminalStyle = "inward_flat_ribbon_l_lead"
        builder.rowTerminalParams = {
            "A1": str(1.5*self.leadThickness),
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.A/2.0),
            "C2": str(self.leadThickness),
        }
        builder.rowTerminalCount = 1
        builder.rowTerminalSpan = self.D

        builder.colTerminalParams = {}
        builder.colTerminalCount = 0
        builder.colTerminalSpan = 0.0

        builder.terminalPitch = 0.0
        builder.terminalTransform = App.Matrix()
        return builder.build()

class MelfBuilder():
    def __init__(self):
        # Params
        self.D = None # min-max
        self.E = None # min-max
        self.L = None # min-max
        # constants
        self.leadThickness = 0.075

    def build(self):
        self.ipcName = "MELF_FIXME"

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "tube"
        builder.bodyParams = {
            "A1": str(self.D - 2*self.L),
            "C1": str(self.E - 2*self.leadThickness),
        }
        builder.bodyTransform = App.Matrix(1, 0, 0, 0,
                                           0, 1, 0, 0,
                                           0, 0, 1, self.leadThickness)

        builder.terminalStyle = "cylindrical_end_cap"
        builder.rowTerminalParams = {
            "A1": str(self.L),
            "C1": str(self.E)
        }
        builder.rowTerminalCount = 1
        builder.rowTerminalSpan = self.D

        builder.colTerminalParams = {}
        builder.colTerminalCount = 0
        builder.colTerminalSpan = 0.0

        builder.terminalPitch = 0.0
        builder.terminalTransform = App.Matrix()
        return builder.build()

class SodBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.A1 = None # min
        self.b = None # min-max
        self.D = None # min-max
        self.D1 = None # min-max
        self.E = None # min-max
        self.L = None # min-max
        # constants
        self.leadThickness = 0.2

    def build(self):
        self.ipcName = "SOD_FIXME"

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D1),
            "B1": str(self.E),
            "C1": str(self.A - self.A1)
        }
        builder.bodyTransform = App.Matrix(1, 0, 0, 0,
                                           0, 1, 0, 0,
                                           0, 0, 1, self.A1)

        builder.terminalStyle = "gull_wing_lead"
        builder.rowTerminalParams = {
            "A1": str(self.D-self.D1),
            "A2": str(self.L),
            "B1": str(self.b),
            "C1": str(self.A1 + (self.A-self.A1)/2.0 + self.leadThickness/2.0),
            "C2": str(self.leadThickness),
        }
        builder.rowTerminalCount = 1
        builder.rowTerminalSpan = self.D

        builder.colTerminalParams = {}
        builder.colTerminalCount = 0
        builder.colTerminalSpan = 0.0

        builder.terminalPitch = 0.0
        builder.terminalTransform = App.Matrix()
        return builder.build()



# FIXME: L != L1, b != b1
class SodflBuilder():
    def __init__(self):
        # Params
        self.A = None # max
        self.b = None # min-max
        self.b1 = None # min-max, optional, default to b
        self.D = None # min-max
        self.D1 = None # min-max
        self.E = None # min-max
        self.L = None # min-max
        self.L1 = None # min-max, optional, default to L
        # constants
        self.leadThickness = 0.2

    def build(self):
        self.ipcName = "SODFL_FIXME"

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D1),
            "B1": str(self.E),
            "C1": str(self.A)
        }
        builder.bodyTransform = App.Matrix()

        builder.terminalStyle = "flat_rectangular_lead"
        builder.rowTerminalParams = {
            "A1": str(self.L),
            "B1": str(self.b),
            "C1": str(self.leadThickness),
        }
        builder.rowTerminalCount = 1
        builder.rowTerminalSpan = self.D

        builder.colTerminalParams = {}
        builder.colTerminalCount = 0
        builder.colTerminalSpan = 0.0

        builder.terminalPitch = 0.0
        builder.terminalTransform = App.Matrix()
        return builder.build()


class Dfn2Builder():
    def __init__(self):
        # Params
        self.A = None # max
        self.b = None # min-max
        self.D = None # min-max
        self.E = None # min-max
        self.e = None # ref
        self.L = None # min-max
        # constants
        self.leadThickness = 0.2

    def build(self):
        self.ipcName = "DFN2_FIXME"

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D),
            "B1": str(self.E),
            "C1": str(self.A)
        }
        builder.bodyTransform = App.Matrix()

        builder.terminalStyle = "flat_rectangular_lead"
        builder.rowTerminalParams = {
            "A1": str(self.L),
            "B1": str(self.b),
            "C1": str(self.leadThickness),
        }
        builder.rowTerminalCount = 1
        builder.rowTerminalSpan = self.e

        builder.colTerminalParams = {}
        builder.colTerminalCount = 0
        builder.colTerminalSpan = 0.0

        builder.terminalPitch = 0.0
        builder.terminalTransform = App.Matrix(1, 0, 0, self.L/2.0,
                                               0, 1, 0, 0,
                                               0, 0, 1, 0,
                                               0, 0, 0, 1)
        return builder.build()

class Dfn4Builder():
    def __init__(self):
        # Params
        self.A = None # max
        self.b = None # ref
        self.D = None # min-max
        self.d = None # ref
        self.E = None # min-max
        self.e = None # min-max
        self.L = None # min-max
        # constants
        self.leadThickness = 0.2

    def build(self):
        self.ipcName = "DFN4_FIXME"

        builder = QilPackageBuilder()
        builder.ipcName = self.ipcName

        builder.bodyStyle = "cube"
        builder.bodyParams = {
            "A1": str(self.D),
            "B1": str(self.E),
            "C1": str(self.A)
        }
        builder.bodyTransform = App.Matrix()

        builder.terminalStyle = "flat_rectangular_lead"
        builder.rowTerminalParams = {
            "A1": str(self.L),
            "B1": str(self.b),
            "C1": str(self.leadThickness),
        }
        builder.rowTerminalCount = 2
        builder.rowTerminalSpan = self.d

        builder.colTerminalParams = {}
        builder.colTerminalCount = 0.0
        builder.colTerminalSpan = 0.0

        builder.terminalPitch = self.e
        builder.terminalTransform = App.Matrix(1, 0, 0, self.L/2.0,
                                               0, 1, 0, 0,
                                               0, 0, 1, 0,
                                               0, 0, 0, 1)
        return builder.build()


# FIXME: Numbering in Z pattern
class SideConcaveBuilder(AbstractBuilder):
    def __init__(self):
        AbstractBuilder.__init__(self)
        # Params
        self.A = None # max
        self.b = None # min-max
        self.D = None # min-max
        self.Dn = None
        self.E = None # min-max
        self.e = None # ref
        self.L = None # min-max

    def build(self):
        self.ipcName = "SC4_FIXME"
        self.doc = newDocument(self.ipcName)
        print self.doc.Name
        self.body = self._makeObject("bodies/cube.fcstd", { "A1": self.E, "B1": self.D, "C1": self.A }, "body")
        dist = Distributor.Distributor(self.Dn, self.e, self.E)
        bot, right, top, left = dist.distribution()
        term = self._makeObject("terminals/side_concave.fcstd", { "A1": self.L, "B1": self.b, "C1": self.A }, "terminal")
        cutout = self._makeObject("terminals/side_concave.fcstd", { "B2": self.b/2.0, "C1": self.A }, "cutout")
        for transform in left + right:
            clone = self.doc.copyObject(term)
            clone.Placement = transform * term.Placement.toMatrix()
            self.terminals.append(clone)
            clone = self.doc.copyObject(cutout)
            clone.Placement = transform * cutout.Placement.toMatrix()
            self.cutouts.append(clone)
        self.doc.removeObject(term.Name)
        self.doc.removeObject(cutout.Name)
        obj = self._buildAll()
        return self.doc



# Chip, array, concave (2 or 4 sides)
class CavBuilder(AbstractBuilder):
    def __init__(self):
        AbstractBuilder.__init__(self)
        # Params
        self.A = None # max
        self.b = None # min-max
        self.b1 = None # min-max, defaults to b
        self.D = None # min-max
        self.Dn = None
        self.E = None # min-max
        self.En = None #
        self.e = None # ref
        self.L = None # min-max
        self.L1 = None # min-max, default to L

    def build(self):
        self.ipcName = "CAV_%dx%d_FIXME" % (self.En, self.Dn)
        self.doc = newDocument(self.ipcName)
        print self.doc.Name
        self.body = self._makeObject("bodies/cube.fcstd", { "A1": self.D, "B1": self.E, "C1": self.A }, "body")
        dist = Distributor.Distributor(self.En, self.e, self.D, self.Dn, self.e, self.E)
        bot, right, top, left = dist.distribution()
        term = self._makeObject("terminals/side_concave.fcstd", { "A1": self.L, "B1": self.b, "C1": self.A }, "terminal")
        cutout = self._makeObject("terminals/side_concave.fcstd", { "B2": self.b/2.0, "C1": self.A }, "cutout")
        for transform in left + bot + right + top:
            clone = self.doc.copyObject(term)
            clone.Placement = transform * term.Placement.toMatrix()
            self.terminals.append(clone)
            clone = self.doc.copyObject(cutout)
            clone.Placement = transform * cutout.Placement.toMatrix()
            self.cutouts.append(clone)
        self.doc.removeObject(term.Name)
        self.doc.removeObject(cutout.Name)
        obj = self._buildAll()
        return self.doc
