= IPC-7351B library

This is a library of electronic component 3D models specially crafted
to work with ECAD (PCB design) using the IPC-7351B standard "Generic
Requirements for Surface Mount Design and Land Pattern Standard"

The 3D models are designed using the nominal package dimensions as per
IPC-7351B. The naming of the 3D model follows the naming of the
associated land pattern (aka. footprint) as per IPC-7351B.

This library follows IPC-7351 recomendations and as a consequence uses
the "Systeme International" unit system.  Which means, that you will
never come across dimensions expressed in inches or mils. Which means
as well that you will never come across an "Imperial" package code, to
further emphasise: here a "0402" package code refers to an IEC
(metric) 0402 package, not an EIA (imperial) 0402 package.

== Design rules guidelines

The approach to build this library is to mix both the electronics
function of the component, it's body shape and it's terminal shape
characteristics to generate a 3D model that looks "good enough", but
is garanteed to match the IPC-7351B dimensionning definitions when it
comes to package body and its terminals.

The 3D package models are simple, and are composed of a body entity
and one entity per terminal, the only colors available for rendering
the package model are:
 - Primary body color
 - Secondary body color
 - Terminal color
 - Terminal "one" color
 - Primary marking color
 - Secondary marking color
 
The goal is not to provide detailed visual model, but to provide
models with accurate dimensions required for designing a PCB.
Typical critical component dimensions when designing a PCB are:
 - Body width, length, height and stand-off
 - Pin width, length and distribution
 
These figures are used for calculating the PCB land pattern (aka footprint):
 - Component courtyard (based on body and pins shape)
 - Pin landing area in the XY plane (For solder joint goals)

Some general rules:
 - Length is along the X Axis, width is along the Y Axis and height is
   along the Z Axis.
 - When dealing with terminal on 4 sides or terminal grid, row means
   along the X axis, column means along the Y axis
 - A *component* (3D model) is the "assembly" of a *body* and a set of
   *terminal*s
 - A *body* model origin is:
   - XY: the center of it's XY extent
   - Z: it's sitting plane
 - A *terminal* model origin is:
   - X: the terminal's furthest point along the X axis
   - Y: the center of it's Y extent
   - Z: the "idealised" terminal sitting plane, landing on the PCB copper surface.
 - A *component* model origin is:
   - XY: the center of it's XY extent (body + terminals)
   - Z: the terminal sitting plane, which may coincide with the body
     sitting plane if the body stand-off is equal to zero
 - A *terminal* model is always represented in it's "Right" form (as
   opposed to it's left, top or bottom PCB placement form), that is:
   the terminal length is "going away" along the X axis
 - A typical component model can easily be created given the
   following parameter:
  - body stand-off (the distance between the PCB surface and the sitting plane of the body)
  - Terminal location and transform (mirror and/or rotation in 2D XY space)

The final component model might be further transformed (rotated) to
achieve the desired IPC-7351 "Pin One" orientation, which is required
for correct pick and place from a reel or tray onto the PCB.

All 3D models are fully parametrics, parameters are as per IPC-7351,
plus extra parameters strictly needed for the purpose of the 3D
models.

To ease parameter usage from FreeCAD features, spreadsheet is embedded
into each model, to capture the various X,Y and Z dimensionning. Due
to FreeCAD spreadsheet implementation, every dimension is named after
the spreadsheet cell's name, that is: A1, A2, A3, ...., B1, B2, B3,
..., which are used this way:
 - The A column is for dimensions along the X axis
 - The B column is for dimensions along the Y axis
 - The C column is for dimensions along the Z axis

== FreeCAD document layout

 - params (spreadsheet)
 - src
   - body
     - ...
   - terminal
     - ...
 - doc
   - drawing
     - ...
 - output
   - fusion
   

=== 2-pins surface-mount components:

==== Body and terminal forms:
 - Chip body with rectangular end cap, polarised and not polarised
 - Molded body with inward flat ribbon L Lead, polarised and not polarised
 - MELF (Metal electrode leadless face) body with Round end cap
 - DFN (Dual flat no lead), with "bottom only" terminal, polarised and not polarised
 - Round can body, with outward L lead (eg: Aluminium Electrolytic Capacitor)
 - Oblong can body, with outward L lead (eg: oscillator)
 
==== Electronic function:
 - capacitor
 - crystal
 - diode
 - ferrite bead
 - fuse
 - inductor
 - LED
 - resistor
 - thermistor
 - varistor
