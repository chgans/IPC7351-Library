#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import sys
sys.path.append("/usr/lib/freecad/lib")

import math, random, unittest

import FreeCAD as App
import FreeCADGui as Gui

# Needed for Shape colors... :(
Gui.showMainWindow()
window = Gui.getMainWindow()
window.hide()


def notImplemented():
    return unittest.skip("Not implemented")

class DistributorTest(unittest.TestCase):

    def test_0by0(self):
        import Distributor
        dist = Distributor.Distributor(0, random.random(), random.random(),
                                       0, random.random(), random.random())
        bot, right, top, left = dist.distribution()
        self.assertEqual(len(bot), 0)
        self.assertEqual(len(top), 0)
        self.assertEqual(len(left), 0)
        self.assertEqual(len(right), 0)

    def test_1by0(self):
        import Distributor
        dist = Distributor.Distributor(1, random.random(), 1.8,
                                       0, random.random(), random.random())

        bot, right, top, left = dist.distribution()
        self.assertEqual(len(bot), 0)
        self.assertEqual(len(top), 0)
        self.assertEqual(len(left), 1)
        self.assertEqual(len(right), 1)

        self.assertEqual(left[0].multiply(App.Vector((1, 1, 1))), App.Vector(-1.9, -1, 1))
        self.assertEqual(right[0].multiply(App.Vector((1, 1, 1))), App.Vector(1.9, 1, 1))

    def test_0by1(self):
        import Distributor
        dist = Distributor.Distributor(0, random.random(), random.random(),
                                       1, random.random(), 1.8)

        bot, right, top, left = dist.distribution()
        self.assertEqual(len(bot), 1)
        self.assertEqual(len(top), 1)
        self.assertEqual(len(left), 0)
        self.assertEqual(len(right), 0)

        self.assertEqual(bot[0].multiply(App.Vector((1, 1, 1))), App.Vector(1, -1.9, 1))
        self.assertEqual(top[0].multiply(App.Vector((1, 1, 1))), App.Vector(-1, 1.9, 1))

    def test_OddByEven(self):
        import Distributor
        dist = Distributor.Distributor(3, 0.1, 1.8, 2, 0.1, 1.8)

        bot, right, top, left = dist.distribution()
        self.assertEqual(len(bot), 2)
        self.assertEqual(len(top), 2)
        self.assertEqual(len(left), 3)
        self.assertEqual(len(right), 3)

        x = 1
        y = 2
        z = 3
        vector = App.Vector((x, y, z))
        self.assertEqual(bot[0].multiply(vector), App.Vector(-(0.1/2.0)+y, -0.9-x, z))
        self.assertEqual(top[0].multiply(vector), App.Vector((0.1/2.0)-y, 0.9+x, z))
        self.assertEqual(bot[1].multiply(vector), App.Vector((0.1/2.0)+y, -x-0.9, z))
        self.assertEqual(top[1].multiply(vector), App.Vector(-(0.1/2.0)-y, 0.9+x, z))
        self.assertEqual(left[0].multiply(vector), App.Vector(-(x+0.9), 0.1-y, z))
        self.assertEqual(right[0].multiply(vector), App.Vector(x+0.9, y-0.1, z))
        self.assertEqual(left[1].multiply(vector), App.Vector(-(x+0.9), 0-y, z))
        self.assertEqual(right[1].multiply(vector), App.Vector(x+0.9, y+0, z))
        self.assertEqual(left[2].multiply(vector), App.Vector(-(x+0.9), -0.1-y, z))
        self.assertEqual(right[2].multiply(vector), App.Vector(x+0.9, y+0.1, z))

@notImplemented()
class DilBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        pass

@notImplemented()
class QilBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        pass


###############################################################################
# IPC-7351B and IPC-7351C input data and foorprint naming
# as per PCB Library Expert Viewer 2017.05 in demo mode
###############################################################################

# BGA
@notImplemented()
class BgaBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        # Pin Number method: Alphanumeric rows
        # Terminal Type: BGA Collapsing Ball
        d=0.50
        e=None
        Dn=11
        En11
        A=1.00
        b=0.30
        D=6.00
        E=6.00

        # BGA121C50P11X11_600X600X100
        # BGA121P50_11X11_600X600X100B30
        pass

# CAE
@notImplemented()
class CaeBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        A=10.20
        b=0.90
        D=None
        D1=10.30
        D2=4.50
        L=3.50

        # CAPAE1030X1020
        # CAPAE1030X1020L350X90
        pass

# CFP
class CfpBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import SowBuilder
        builder = SowBuilder()
        builder.A = 2.50
        builder.A1 = 0.25
        builder.D = 11.02
        builder.E = 11.30
        builder.E1 = 7.10
        builder.L = 1.085
        builder.e = 1.27
        builder.b = 0.43
        builder.Dn = 8
        self.assertIsNotNone(builder.build())

        # CFP127P1130X250-16
        # CFP16P127_1102X1130X250L108X43

# CQFP (Same as QFP)
class CqfpBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import QfpBuilder
        builder = QfpBuilder()
        builder.A = 4.92
        builder.A1 = 0.51
        builder.D = 22.35
        builder.D1 = 17.28
        builder.E = 22.35
        builder.E1 = 17.28
        builder.L = 2.0
        builder.e = 1.27
        builder.b = 0.265
        builder.Dn = 13
        builder.En = 13
        self.assertIsNotNone(builder.build())

        # CQFP127P2235X2235X492-52
        # CQFP52P127_2235X2235X492L114X26

# Chip
class ChipBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import ChipBuilder
        builder = ChipBuilder()
        builder.A = 1.40
        builder.D = 3.20
        builder.E = 1.60
        builder.L = 0.50
        builder.L1 = 0.60
        self.assertIsNotNone(builder.build())

        # Family: resistor
        # RESC3216X140
        # RESC320X150X140L50

# CAF/CAV 2 sides
class Cav2BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import CavBuilder
        builder = CavBuilder()
        builder.A = 0.70
        builder.b = 0.80
        builder.D = 5.08
        builder.Dn = 4
        builder.E = 2.10
        builder.En = 0
        builder.e = 1.27
        builder.L = 0.40
        self.assertIsNotNone(builder.build())

        # Lead shape: concave
        # Family: resistor
        # RESCAV127P508X210X70-8
        # RESCAV8P127_508X210X40L40X80


# CAX 2 sides
@notImplemented()
class CaxBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        # Family: resistor
        e=0.64
        Dn=5
        A=0.70
        b=0.35
        b1=0.50
        D=3.30
        E=2.10
        L=0.45

        # RESCAXS64P330X210X70-10
        # RESCAXS10P64_330X210X70L45X35
        pass

# CAF/CAV 4 sides
class Cav4BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import CavBuilder
        builder = CavBuilder()
        builder.A = 0.65
        builder.b = 0.40
        builder.b1 = 0.50
        builder.D = 4.0
        builder.Dn = 4
        builder.E = 2.10
        builder.En = 1
        builder.e = 0.8
        builder.L = 0.40
        builder.L1 = 0.45
        self.assertIsNotNone(builder.build())

        # Family: resistor
        # Lead Shape: concave
        # RESCAV80P400X210X65-10
        # RESCAV10P80_400X210X65L40X40

# CGA
@notImplemented()
class CgaBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        # Pin number method: alphanumeric rows
        d=1.27
        e=None
        A=5.41
        A1=2.36
        b=0.51
        D=32.50
        E=32.50

        # CGA625C127P25X25_3250X3250X541
        # CGA625CP127_25X25_3250X3250X541L51
        pass

# XTAL
@notImplemented()
class XtalBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        A=3.50
        b=0.645
        D=13.00
        D1=11.50
        D2=4.68
        E=4.90
        L=None

        # XTAL1150X490X350
        # XTAL1150X490X350D1300L416X64
        pass

# DFN 2 pins
class Dfn2BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import Dfn2Builder
        builder = Dfn2Builder()
        builder.A = 0.50
        builder.b = 0.505
        builder.D = 1.00
        builder.E = 0.60
        builder.e = 0.65
        builder.L = 0.25
        self.assertIsNotNone(builder.build())

        # DIODFN100X60X50-2
        # DIODFN2_100X60X50L25X50

# DFN 3 pins
@notImplemented()
class Dfn3BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import Dfn3Builder
        builder = Dfn3Builder()
        builder.A = 3.12
        builder.b = 2.415
        builder.b1 = 5.715
        builder.D = 10.16
        builder.d = 5.52
        builder.E = 7.52
        builder.e = 3.81
        builder.L = 3.05
        builder.L1 = 6.225
        self.assertIsNotNone(builder.build())

        # DIODFN1016X752X312-3
        # DIODFN3_1016X752X312L305X241

# DFN 4 pins
class Dfn4BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import Dfn4Builder
        builder = Dfn4Builder()
        builder.A = 0.40
        builder.b = 0.255
        builder.D = 1.20
        builder.d = 0.75
        builder.E = 0.775
        builder.e = 0.45
        builder.L = 0.35
        self.assertIsNotNone(builder.build())

        # DIODFN120X77X40-4
        # DIODFN4_120X77X40L35X25

# DPAK
@notImplemented()
class DpakBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        # Pitch (e): 2.54
        # Pins: 4
        A=2.03
        b=0.71
        D=10.54
        D1=7.015
        D2=6.69
        E=8.395
        E1=5.79
        L=0.915
        L1=1.01

        # DPAK254P1054X203-4
        # DPAK4P254_1054X203L91X71T669X579
        pass

# LCC
@notImplemented()
class LccBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e=1.27
        Dn=7
        En=7
        A=2.54
        b=0.635
        D=11.43
        E=11.43
        L=0.905
        L1=1.78

        # LCC127P1143X1143X254-28
        # LCC28P127_1143X1143X254L90X63
        pass

@notImplemented()
class LccsBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        pass

# LGA
@notImplemented()
class LgaBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        # Pin umber method: Alphanumeric rows
        # Lead shape: Round
        d=0.50
        e=None
        Dn=11
        En=11
        A=1.00
        b=0.25
        D=6.00
        E=6.00

        # LGA121C50P11X11_600X600X100
        # LGA121CP50_11X11_600X600X100L25
        pass

# MELF
class MelfBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import MelfBuilder
        builder = MelfBuilder()
        builder.D = 3.50
        builder.E = 1.525
        builder.L = 0.425
        self.assertIsNotNone(builder.build())

        # Family: Diode
        # DIOMELF3515
        # DIOMELF250X152L42

# Molded
class MoldedBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import MoldedBuilder
        builder = MoldedBuilder()
        builder.A = 3.10
        builder.b = 2.40
        builder.D = 7.30
        builder.E = 4.30
        builder.L = 1.30
        #builder.L1 = 1.50
        self.assertIsNotNone(builder.build())

        # Family: Capacitor, Polarised
        # CAPPM7343X310
        # CAPPM730X430X310L130X240

# OSCCC
@notImplemented()
class OscccBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        A=2.00
        D=6.50
        D1=3.00
        E=4.00
        E1=1.50

        # OSCC650X400X200
        # OSCC650X400X200L125X175
        pass

# OSCJ
@notImplemented()
class OscjBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e=5.08
        Dn=2
        A=3.56
        b=0.50
        D=14.00
        E=9.60
        E1=8.60
        E2=7.62

        # OSCJ508P1400X860X356-4
        # OSCJ4P208_1400X860X356L198X50
        pass

# OSCL
@notImplemented()
class OsclBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e=5.08
        Dn=2
        A=4.70
        b=0.50
        D=14.00
        E=9.60
        E1=8.95
        E2=7.62

        # OSCL508P1400X895X470-4
        # OSCL4P508_1400X895X470L198X50
        pass

# OSCSC
@notImplemented()
class OscscBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e=2.54
        Dn=3
        A=1.70
        b=1.40
        D=7.00
        E=5.00
        L=1.00

        # OSCSC254P700X500X170-6
        # OSCSC6P254_700X500X170L100X140
        pass

# PLCC
class PlccBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import PlccBuilder
        builder = PlccBuilder()
        builder.A = 4.57
        builder.D = 12.445
        builder.D1 = 11.50
        builder.D2 = 10.60
        builder.E = 12.445
        builder.E1 = 11.50
        builder.e = 1.27
        builder.b = 0.43
        builder.Dn = 7
        builder.En = 7
        self.assertIsNotNone(builder.build())

        # PLCC127P1244X1244X457-28
        # PLCC28P127_1244X1244X457L184X43

# PLCCS
@notImplemented()
class PlccsBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        pass

# QFN
class QfnBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import QfnBuilder
        builder = QfnBuilder()
        builder.A = 0.80
        builder.D = 5.00
        builder.E = 4.00
        builder.L = 0.40
        builder.e = 0.50
        builder.b = 0.25
        builder.Dn = 8
        builder.En = 6
        self.assertIsNotNone(builder.build())

        # Lead shape: D-shape
        # QFN50P500X400X80-28
        # QFN28P50_500X400X80L40X25

# QFN+T
@notImplemented()
class QfnWithTabBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        # Lead shape: D-shape
        e=0.50
        Dn=8
        En=6
        A=0.80
        b=0.25
        D=5.00
        D2=3.55
        E=4.00
        E2=2.55
        L=0.40
        tc=0.35
        tr=0.05

        # QFN50P500X400X80-29
        # QFN29P50_500X400X80L40X25T355X255
        pass

# PQFN
class PqfnBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import PqfnBuilder
        builder = PqfnBuilder()
        builder.A = 0.80
        builder.D = 5.00
        builder.E = 4.00
        builder.L = 0.40
        builder.L1 = 0.10
        builder.e = 0.50
        builder.b = 0.25
        builder.Dn = 7
        builder.En = 5
        self.assertIsNotNone(builder.build())

        # Lead shape: D-shape
        # PQFN50P500X400X80-24
        # PQFN24P50_500X400X80L40X25

# PQFN+T
@notImplemented()
class PqfnWithTabBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        # Lead shape: D-shape
        e=0.50
        Dn=7
        En=5
        A=0.8
        b=0.25
        D=5.00
        D2=3.30
        E=4.00
        E2=2.30
        L=0.40
        L1=0.10
        tc=0.25
        tr=0.05

        # PQFN50P500X400X80-25
        # PQFN25P50_500X400X80L40X25T330X230
        pass

# QFP
class QfpBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import QfpBuilder
        builder = QfpBuilder()
        builder.A = 1.20
        builder.A1 = 0.05
        builder.D = 12.90
        builder.D1 = 10.00
        builder.E = 12.90
        builder.E1 = 10.00
        builder.L = 0.905
        builder.e = 0.50
        builder.b = 0.22
        builder.Dn = 16
        builder.En = 16
        self.assertIsNotNone(builder.build())

        # QFP50P1290X1290X120-64
        # QFP64P50_1290X1290X120L90X22

# QFP+T
@notImplemented()
class QfpWithTabBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e=0.50
        Dn=16
        En=16
        A=1.2
        A1=0.05
        b=0.22
        D=12.90
        D1=10.00
        D2=7.00
        E=12.90
        E1=10.00
        E2=7.00
        L=0.905
        tc=None
        tr=None

        # QFP50P1290X1290X120-65
        # QFP65P50_1290X1290X120L90X22T700
        pass

# SC2
@notImplemented()
class SC2BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        pass

# SC4
class SC4BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import SideConcaveBuilder
        builder = SideConcaveBuilder()
        builder.A = 0.80
        builder.b = 0.70
        builder.D = 2.05
        builder.Dn = 4
        builder.E = 1.60
        builder.e = 0.95
        builder.L = 0.30
        self.assertIsNotNone(builder.build())

        # DIOSC95P205X160X80-4
        # DIOSCP95_205X160X80L30

# SOD
class SodBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import SodBuilder
        builder = SodBuilder()
        builder.A = 1.27
        builder.A1 = 0.01
        builder.b = 0.65
        builder.D = 3.75
        builder.D1 = 2.70
        builder.E = 1.60
        builder.L = 0.285
        self.assertIsNotNone(builder.build())

        # SOD3716X127
        # SOD270X160X127L28X65

# SODFL
class SodflBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import SodflBuilder
        builder = SodflBuilder()
        builder.A = 0.70
        builder.b = 0.3
        builder.D = 1.60
        builder.D1 = 1.2
        builder.E = 0.8
        builder.L = 0.2
        self.assertIsNotNone(builder.build())

        # SODFL1608X70
        # SODFL120X80X70L20X30

# SOIC/SOP
class SoicBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import SowBuilder
        builder = SowBuilder()
        builder.A = 1.20
        builder.A1 = 0.05
        builder.D = 7.80
        builder.E = 6.40
        builder.E1 = 4.40
        builder.L = 0.60
        builder.e = 0.65
        builder.b = 0.245
        builder.Dn = 12
        self.assertIsNotNone(builder.build())

        # SOP65P640X120-24
        # SOP24P65_780X640X120L60X24

# SOIC/SOP+T
class SoicWithTabBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e = 0.65
        Dn=12
        A=1.20
        A1=0.05
        b=0.245
        D=7.80
        D2=4.30
        E=6.40
        E1=4.40
        E2=3.00
        L=0.60
        tc=None
        tr=None

        # SOP65P640X120-25
        # SOP25P65_780X640X120L60X24T430X300
        pass

# SOJ
class SojBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import SojBuilder
        builder = SojBuilder()
        builder.A = 3.56
        builder.D = 12.8
        builder.E = 8.66
        builder.E1 = 7.52
        builder.E2 = 6.80
        builder.e = 1.27
        builder.b = 0.435
        builder.Dn = 10
        self.assertIsNotNone(builder.build())

        # SOJ127P866X356-20
        # SOJ20P127_1280X866X356L43

# SON
class SonBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import SonBuilder
        builder = SonBuilder()
        builder.A = 0.80
        builder.D = 4.00
        builder.E = 3.00
        builder.L = 0.40
        builder.L1 = 0.60
        builder.e = 0.50
        builder.b = 0.25
        builder.Dn = 7
        self.assertIsNotNone(builder.build())

        # SON50P400X300X80-14
        # SON14P50_400X300X80L40X25

# SON+T
@notImplemented()
class SonWithTabBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        # Lead shape: d-shape
        A=0.80
        b=0.25
        D=4.00
        D2=3.10
        E=4.00
        E2=2.30
        L=0.50
        L2=0.10
        tc=0.25
        tr=0.10

        # SON50P400X300X80-15
        # SON15P50_400X300X80L40X25T340X150
        pass

# PSON
class PsonBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import PsonBuilder
        builder = PsonBuilder()
        builder.A = 0.8
        builder.b = 0.25
        builder.D = 4.00
        builder.E = 4.00
        builder.L = 0.50
        builder.L1 = 0.70
        builder.L2 = 0.10
        builder.e = 0.50
        builder.Dn = 7
        self.assertIsNotNone(builder.build())

        # PSON50P400X400X80-14
        # PSON14P50_400X400X80L50X25

# PSON+T
@notImplemented()
class PsonWithTabBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e=0.50
        A=0.80
        b=0.25
        D=4.00
        D2=3.10
        E=4.00
        E2=2.30
        L=0.50
        L2=0.10
        tc=0.25
        tr=0.10

        # PSON50P400X400X80-15
        # PSON15P50_400X400X80L50X25T310X230
        pass


# SOFL
@notImplemented()
class SoflBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        # Family: transistor
        e=0.5
        pinCount=5
        A=0.60
        b=0.225
        D=1.60
        E=1.60
        E1=1.20
        L=0.35

        # SOT50P160X60-5
        # SOTFL5P50_160X60L35X22
        pass

# SOL
class SolBuilderTest(unittest.TestCase):
    def test_canBuild(self):
        from IpcPackageBuilder import SolBuilder
        builder = SolBuilder()
        builder.A = 1.10
        builder.D = 2.00
        builder.E = 2.22
        builder.E1 = 1.70
        builder.E2 = 1.65
        builder.e = 0.50
        builder.b = 0.225
        builder.Dn = 4
        self.assertIsNotNone(builder.build())

        # SOL50P220X110-8
        # SOL8P50_200X220X110L22

# SOT23
@notImplemented()
class Sot23BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e=0.50
        pinCount=3
        A=1.10
        A1=0.13
        b=0.44
        D=2.90
        E=2.40
        E1=1.30
        L=0.53

        # SOT95P240X110-3
        # SOT23-3P95_240X110L53X44
        pass

# SOT143
@notImplemented()
class Sot143BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e=1.92
        A=1.12
        A1=0.01
        b=0.435
        b1=0.825
        D=2.92
        E=2.37
        E1=1.30
        L=0.50

        # SOT192P237X112-4
        # SOT143-4P192_237X112L50X43N
        pass

# SOT223
@notImplemented()
class Sot223BuilderTest(unittest.TestCase):
    def test_canBuild(self):
        e=2.30
        pinCount=4
        A=1.80
        A1=0.10
        b=0.70
        b1=3.00
        D=6.50
        E=7.00
        L=0.90

        # SOT230P700X180-4
        # SOT223-4P230_700X180L90X70
        pass

if __name__ == '__main__':
    unittest.main()
