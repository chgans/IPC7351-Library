#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

proj_path = os.getcwd()

# This is so Django knows where to find stuff.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
sys.path.append(proj_path)

# This is so my local_settings.py gets loaded.
os.chdir(proj_path)

# This is so models get loaded.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

refFileName=sys.argv[1]

import re

input = {}
with open(refFileName) as file:
    for line in file.readlines():
        m = re.match(r"^(?P<desc>[^.]+[^ .])[ .]*(?P<scheme>.*)$", line)
        desc = m.group(1)
        scheme = m.group(2)
        replacements = (
            ('Body Length X Body Width X Height', '${BodyLength} X ${BodyWidth} X ${BodyHeight}'),
            ('Body Length + Body Width X Height', '${BodyLength} + ${BodyWidth} X ${BodyHeight}'),
            ('Body Length + Body Diameter', '${BodyLength} + ${BodyDiameter}'),
            ('Base Body Size X Height', '${BaseBodySize} X ${BodyHeight}'),

            ('Lead Span L1 X Lead Span L2 Nominal X Height', '${LeadSpanL1} X ${LeadSpanL2} X ${BodyHeight}'),
            ('Lead Span Nominal X Height', '${LeadSpan} X ${BodyHeight}'),
            ('Lead Span Nominal + Body Width X Height', '${LeadSpan} + ${BodyWidth} X ${BodyHeight}'),
            ('Lead Span X Height', '${LeadSpan} X ${Height}'),

            ('Thermal Pad', '${ThermalPad}'),

            ('Ball Columns', '${BallColumns}'),
            ('Ball Rows', '${BallRows}'),
            ('Ball Diameter', '${BallDiameter}'),
            ('+ C or N +', '+ ${BallStyle} +'),
            ('Pin Columns', '${PinColumns}'),
            ('Pin Rows', '${PinRows}'),
            ('Col Pitch X Row Pitch P', '${ColPitch} X ${RowPitch} P'),
            ('Pitch P', '${Pitch} P'),
            ('Pin Qty', '${PinQty}'),

            ('+', ''),
            (' ', ''),
        )

        # The text describe the same component, but has different wording in the C and B table
        fixups = (
            (u"\u2010", '-'), # Hyphen
            (u"\u2013", '-'), # En Dash
            ('Diodes, Non-polarized Chip', 'Diodes, Non-polarized, Chip'),
            ('Diodes, Non-polarized Molded', 'Diodes, Non-polarized, Molded'),
            ('TO (Generic DPAK - Example: TO228P970X238-3)', 'DPAK (TO)'),
            ('Small Outline Transistors, Flat Lead', 'Transistor, Small Outline, Flat Lead'),
            ('Small Outline Diodes, Flat Lead', 'Diodes, Small Outline, Flat Lead'),
            ('SOD (Example: SOD3717X135 = JEDEC SOD123)', 'Diodes, Small Outline'),
            ('LED’s, Side Concave, 2 Pin', 'LED’s, Side Concave'),
            ('Diodes, Side Concave, 2 Pin', 'Diodes, Side Concave'),
        )

        # Doesn't exists in C, but is actually redundant in B
        forgets = (
            # The 'R' at the end is handle by both B and C naming convention
            # "pin order and pin quantity modifier", no need for a special entry
            'SOT143 & SOT343 Reverse (JEDEC Standard Package)',
        )

        # Single entry in B that have several entries in C, B can accept all C cases
        duplicates = {
            'Diodes, Small Outline, Flat Lead': ('Diodes, Small Outline, Flat Lead, 2 Pin',
                                                 'Diodes, Small Outline, Flat Lead, 3 - 6 Pin'),
            'Diodes, Dual Flat No-lead': ('Diodes, Dual Flat No-lead',
                                          'Diodes, Non-polarized, Dual Flat No-lead'),
            'SOT143 & SOT343 (JEDEC Standard Package)': ('SOT143',
                                                         'SOT343'),
            'SOT23 & SOT223 Packages (Example: SOT230P700X180-4)': ('SOT23',
                                                                    'SOT223'),
        }
        if desc in forgets:
            continue

        for input, output in replacements:
            scheme = scheme.replace(input, output)            

        for input, output in fixups:
            scheme = scheme.replace(input, output)
            desc = desc.replace(input, output)

        if desc in duplicates.keys():
            for dupdesc in duplicates[desc]:
                print("%s|%s" % (dupdesc, scheme))
        else:
            print("%s|%s" % (desc, scheme))

# TBD: Entries in C that don't have any equivalent in B:
# IC, Small Outline Package, Flat Lead (SOPFL)
