from django.contrib import admin

from .models import ComponentTemplate, ParameterTemplate, OptionTemplate

class OptionTemplateAdmin(admin.ModelAdmin):
    list_display = ( 'name', 'component', 'isOptional')

admin.site.register(OptionTemplate, OptionTemplateAdmin)

class ParameterTemplateAdmin(admin.ModelAdmin):
    list_display = ( 'name', 'component', 'isOptional', 'isNominalRequired', 'isMinimumRequired', 'isMaximumRequired')
    list_filter = ('name', 'component')

admin.site.register(ParameterTemplate, ParameterTemplateAdmin)

class ParameterTemplateInline(admin.TabularInline):
    model = ParameterTemplate
    extra = 10

class OptionTemplateInline(admin.TabularInline):
    model = OptionTemplate
    extra = 5

class ComponentTemplateAdmin(admin.ModelAdmin):
    list_display = ('name', 'dimensionImage')
    list_filter = ('name', 'dimensionImage')
    fieldsets = [
        (None, {'fields': ['name', 'dimensionImage']}),
    ]
    inlines = [ OptionTemplateInline, ParameterTemplateInline ]



admin.site.register(ComponentTemplate, ComponentTemplateAdmin)
