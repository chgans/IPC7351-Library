from django.db import models

class ComponentTemplate(models.Model):
    name = models.CharField(max_length=128)
    dimensionImage = models.ImageField(upload_to='components/dimensioning/')
    def __str__(self):
        return self.name

# Code: E, name: FK(ParameterName) (eg: BodyLength)
class ParameterTemplate(models.Model):
    component = models.ForeignKey(ComponentTemplate)
    name = models.CharField(max_length=128)
    isMinimumRequired = models.BooleanField(default=True)
    isMaximumRequired = models.BooleanField(default=True)
    isNominalRequired = models.BooleanField(default=False)
    isOptional = models.BooleanField(default=False)

    def __str__(self):
        return self.name

# Code: e, name: FK(OptionName) (eg, Pitch)
class OptionTemplate(models.Model):
    component = models.ForeignKey(ComponentTemplate)
    name = models.CharField(max_length=128)
    isOptional = models.BooleanField(default=False)

    def __str__(self):
        return self.name
