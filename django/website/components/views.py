from django.shortcuts import get_object_or_404, get_list_or_404, render

from .models import ComponentTemplate, OptionTemplate, ParameterTemplate

def index(request):
    return HttpResponse("Hello, world. You're at the components index.")


def component(request, component_id):
    return render(request, 'components/component.html', {
        'component': get_object_or_404(ComponentTemplate, pk=component_id),
        'option_list' : get_list_or_404(OptionTemplate, component_id=component_id),
        'parameter_list': get_list_or_404(ParameterTemplate, component_id=component_id)
    })
