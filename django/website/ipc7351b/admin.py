from django.contrib import admin

from .models import ComponentFamily, ComponentCategory, NamingScheme
from .models import TerminalStyle, LandShape, Variable

class ComponentFamilyAdmin(admin.ModelAdmin):
    list_display = ('code', 'description')

admin.site.register(ComponentFamily, ComponentFamilyAdmin)

class ComponentCategoryAdmin(admin.ModelAdmin):
    list_display = ('code', 'description')

admin.site.register(ComponentCategory, ComponentCategoryAdmin)

class NamingSchemeAdmin(admin.ModelAdmin):
    list_display = ('family', 'category', 'revision', 'prefix', 'scheme')
    list_filter = ('family', 'category', 'revision', 'scheme')

admin.site.register(NamingScheme, NamingSchemeAdmin)

class TerminalStyleAdmin(admin.ModelAdmin):
    list_display = ('code', 'description')

admin.site.register(TerminalStyle, TerminalStyleAdmin)

class VariableAdmin(admin.ModelAdmin):
    list_display = ('code', 'description')

admin.site.register(Variable, VariableAdmin)

class LandShapeAdmin(admin.ModelAdmin):
    list_display = ('code', 'description')

admin.site.register(LandShape, LandShapeAdmin)
