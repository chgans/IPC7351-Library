from django.db import models

REVISIONB = 'B'
REVISIONC = 'C'
REVISION_CHOICES = (
    (REVISIONB, 'Revision B'),
    (REVISIONC, 'Revision C')
)

class ComponentFamily(models.Model):
    code = models.CharField(max_length = 10, unique=True)
    description = models.CharField(max_length = 100)

    def __str__(self):
        return self.description

class ComponentCategory(models.Model):
    code = models.CharField(max_length = 10)
    description = models.CharField(max_length = 100)

    def __str__(self):
        return self.description

class NamingScheme(models.Model):
    family = models.ForeignKey(ComponentFamily)
    category = models.ForeignKey(ComponentCategory)
    revision = models.CharField(max_length = 1, choices = REVISION_CHOICES)
    scheme = models.CharField(max_length = 256)

    def prefix(self):
        return self.family.code + self.category.code

    def __str__(self):
        return self.family.code + self.category.code

class TerminalStyle(models.Model):
    code = models.CharField(max_length = 10, unique=True)
    description = models.CharField(max_length = 100)

    def __str__(self):
        return self.description

class Variable(models.Model):
    code = models.CharField(max_length = 32, unique=True)
    description = models.CharField(max_length = 100)

    def __str__(self):
        return self.description

class LandShape(models.Model):
    code = models.CharField(max_length = 10, unique=True)
    description = models.CharField(max_length = 100)

    def __str__(self):
        return self.description
