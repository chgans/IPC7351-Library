# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-31 08:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ipc7351b', '0002_option_variable'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Option',
        ),
    ]
