from django.contrib import admin

from .models import Component, Manufacturer, NamingVariable
from .models import ComponentTemplate, DimensionTemplate, OptionTemplate
from .models import ComponentInstance, DimensionInstance, OptionInstance


class ComponentAdmin(admin.ModelAdmin):
    list_display = ('pk', 'description', 'templateCount')
admin.site.register(Component, ComponentAdmin)

class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('pk', 'code', 'name')
    search_fields = [ 'code', 'name' ]
admin.site.register(Manufacturer, ManufacturerAdmin)

class NamingVariableAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'description')

admin.site.register(NamingVariable, NamingVariableAdmin)


class DimensionTemplateInline(admin.TabularInline):
    model = DimensionTemplate
    extra = 5

class OptionTemplateInline(admin.TabularInline):
    model = OptionTemplate
    extra = 5

class ComponentTemplateAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'componentCount', 'dimensionDrawing')
    fieldsets = [
        (None, {'fields': ['name', 'dimensionDrawing', 'components']}),
    ]
    inlines = [ OptionTemplateInline, DimensionTemplateInline ]
    search_fields = [ 'components__description', 'dimensionDrawing', 'name']
    pass

admin.site.register(ComponentTemplate, ComponentTemplateAdmin)
admin.site.register(DimensionTemplate)

class DimensionInstanceInline(admin.TabularInline):
    model = DimensionInstance
    extra = 10

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(DimensionInstanceInline, self).formfield_for_foreignkey(db_field, request, **kwargs)
        if db_field.name == 'dimensionTemplate':
            print(request._obj_)
            if request._obj_ is not None:
                field.queryset = field.queryset.filter(componentTemplate__exact = request._obj_.componentTemplate)
            else:
                field.queryset = field.queryset.none()
        return field

class OptionInstanceInline(admin.TabularInline):
    model = OptionInstance
    extra = 10

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(OptionInstanceInline, self).formfield_for_foreignkey(db_field, request, **kwargs)
        if db_field.name == 'dimensionTemplate':
            print(request._obj_)
            if request._obj_ is not None:
                field.queryset = field.queryset.filter(componentTemplate__exact = request._obj_.componentTemplate)
            else:
                field.queryset = field.queryset.none()
        return field

class ComponentInstanceAdmin(admin.ModelAdmin):
    inlines = [ OptionInstanceInline, DimensionInstanceInline ]

    def get_form(self, request, obj=None, **kwargs):
        # just save obj reference for future processing in Inline
        request._obj_ = obj
        request._obj_ = obj
        return super(ComponentInstanceAdmin, self).get_form(request, obj, **kwargs)

admin.site.register(ComponentInstance, ComponentInstanceAdmin)

class DimensionInstanceAdmin(admin.ModelAdmin):
    pass
admin.site.register(DimensionInstance, DimensionInstanceAdmin)
