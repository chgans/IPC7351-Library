from django.db import models
from django.core.exceptions import ValidationError

class Component(models.Model):
    description = models.CharField(max_length=256)
    namingRevisionB = models.CharField(max_length=256)
    namingRevisionC = models.CharField(max_length=256)

    class Meta:
        ordering = [ 'description' ]

    def templateCount(self):
        return self.componentTemplates.count()

    def __str__(self):
        return self.description

class Manufacturer(models.Model):
    code = models.CharField(max_length=32)
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name

class NamingVariable(models.Model):
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=256)

    class Meta:
        ordering = [ 'name' ]

    def __str__(self):
        return self.name

class ComponentTemplate(models.Model):
    name = models.CharField(max_length=64)
    components = models.ManyToManyField(Component, related_name='componentTemplates')
    dimensionDrawing = models.ImageField(upload_to='components/dimensioning/')

    def componentCount(self):
        return self.components.count()

    def __str__(self):
        return self.name

class DimensionTemplate(models.Model):
    componentTemplate = models.ForeignKey(ComponentTemplate, related_name='dimensionTemplates')
    dimension = models.CharField(max_length=64)
    namingVariable = models.ForeignKey(NamingVariable, blank=True, null=True, related_name='dimensionTemplates')
    isOptional = models.BooleanField(default=False)
    isNominalRequired = models.BooleanField(default=False)
    isMinimumRequired = models.BooleanField(default=True)
    isMaximumRequired = models.BooleanField(default=True)

    def __str__(self):
        return "%s=%s" % (self.dimension, self.namingVariable.name if self.namingVariable is not None else "None")

class OptionTemplate(models.Model):
    componentTemplate = models.ForeignKey(ComponentTemplate, related_name='optionTemplates')
    option = models.CharField(max_length=64)
    namingVariable = models.ForeignKey(NamingVariable, blank=True, null=True, related_name='optionTemplates')
    isOptional = models.BooleanField(default=False)

    def __str__(self):
        return "%s=%s" % (self.option, self.namingVariable.name if self.namingVariable is not None else "None")

class ComponentInstance(models.Model):
    manufacturer = models.ForeignKey(Manufacturer, blank=True, null=True)
    packageCode = models.CharField(max_length=128, blank=True)
    dataSource = models.CharField(max_length=128, blank=True)
    component = models.ForeignKey(Component, related_name = 'componentInstances')
    componentTemplate = models.ForeignKey(ComponentTemplate, related_name = 'componentInstances')

    def clean(self):
        errors = {}
        for dimensionInstance in self.dimensionInstances.all():
            try:
                dimensionInstance.clean()
            except ValidationError as dimensionErrors:
                for field in dimensionErrors.message_dict.keys():
                    errors[dimensionInstance.dimensionTemplate.dimension + "." + field] = dimensionErrors.message_dict[field]
        if len(errors):
            raise ValidationError(errors)

class DimensionInstance(models.Model):
    componentInstance = models.ForeignKey(ComponentInstance, related_name = 'dimensionInstances')
    dimensionTemplate = models.ForeignKey(DimensionTemplate, related_name = 'dimensionTemplates')
    nominalValue = models.CharField(max_length=64, blank=True)
    minimumValue = models.CharField(max_length=64, blank=True)
    maximumValue = models.CharField(max_length=64, blank=True)

    def clean(self):
        errors = {}
        if len(self.nominalValue) == 0 and self.dimensionTemplate.isNominalRequired and not self.dimensionTemplate.isOptional:
            errors['nominalValue'] = 'Value required'
        if len(self.minimumValue) == 0 and self.dimensionTemplate.isMinimumRequired and not self.dimensionTemplate.isOptional:
            errors['minimumValue'] = 'Value required'
        if len(self.maximumValue) == 0 and self.dimensionTemplate.isMaximumRequired and not self.dimensionTemplate.isOptional:
            errors['maximumValue'] = 'Value required'
        if errors.keys():
            raise ValidationError(errors)

class OptionInstance(models.Model):
    componentInstance = models.ForeignKey(ComponentInstance, related_name = 'optionInstances')
    optionTemplate = models.ForeignKey(DimensionTemplate, related_name = 'optionTemplates')
    value = models.CharField(max_length=64, blank=True)
