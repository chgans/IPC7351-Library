# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-03 01:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ipc7351', '0011_manufacturer'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComponentInstance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('packageCode', models.CharField(blank=True, max_length=128)),
                ('dataSource', models.CharField(blank=True, max_length=128)),
                ('component', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='componentInstances', to='ipc7351.Component')),
                ('componentTemplate', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='componentInstances', to='ipc7351.ComponentTemplate')),
                ('manufacturer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ipc7351.Manufacturer')),
            ],
        ),
    ]
