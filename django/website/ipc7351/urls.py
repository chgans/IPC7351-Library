from django.conf.urls import url

from . import forms
from . import views

urlpatterns = [
    url(r'^new-component-instance/$', views.NewComponentInstanceWizard.as_view([forms.NewComponentInstance1, forms.NewComponentInstance2])),
]
