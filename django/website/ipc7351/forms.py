from django import forms
from . import models

class NewComponentInstance1(forms.ModelForm):
    class Meta:
        model = models.ComponentInstance
        fields = ['componentTemplate']

class NewComponentInstance2(forms.ModelForm):
    class Meta:
        model = models.ComponentInstance
        fields = ['component']
#        widgets = {
#            'component': forms.ModelMultipleChoiceField(queryset=models.Component.objects.filter())
#        }

