from django.test import TestCase

from .models import Component, NamingVariable
from .models import ComponentTemplate, DimensionTemplate
from .models import ComponentInstance, DimensionInstance

class Ipc7351TestCase(TestCase):
    fixtures = [ 'ipc7351-dataset.yaml' ]

    def test_tdd_relation_names(self):
        _ = Component.objects.first().componentTemplates.all()
        _ = ComponentTemplate.objects.first().components.all()
        _ = ComponentTemplate.objects.first().dimensionTemplates.all()
        _ = DimensionTemplate.objects.first().componentTemplate
        _ = NamingVariable.objects.first().dimensionTemplates.all()

    def test_generator(self):
        ct = ComponentTemplate.objects.get(name='Chip')
        print(ct.components.all())
        c = ct.components.first()
        ci = ComponentInstance(component=c, componentTemplate=ct)
        builder.A = 1.40
        builder.D = 3.20
        builder.E = 1.60
        builder.L = 0.50
        builder.L1 = 0.60
