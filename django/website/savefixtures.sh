mkdir -p ipc7351/fixtures/
python manage.py dumpdata --format yaml ipc7351 > ipc7351/fixtures/ipc7351-dataset.yaml
mkdir -p ipc7351b/fixtures/
python manage.py dumpdata --format yaml ipc7351b > ipc7351b/fixtures/ipc7351b-dataset.yaml
mkdir -p components/fixtures/
python manage.py dumpdata --format yaml components > components/fixtures/components-dataset.yaml
