#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

proj_path = os.getcwd()

# This is so Django knows where to find stuff.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
sys.path.append(proj_path)

# This is so my local_settings.py gets loaded.
os.chdir(proj_path)

# This is so models get loaded.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

refFileName=sys.argv[1]

import re

input = {}
with open(refFileName) as file:
    for line in file.readlines():
        m = re.match(r"^(?P<desc>[^.]+[^ .])[ .]*(?P<scheme>.*)$", line)
        desc = m.group(1)
        scheme = m.group(2)
        replacements = (
            ('L Lead Length X Width', 'L ${LeadLength} X {LeadWidth}'),
            ('L Lead Length X Lead Width', 'L ${LeadLength} X {LeadWidth}'),
            ('L Diameter', 'L ${LeadDiameter}'),
            ('L Lead Diameter', 'L ${LeadDiameter}'),
            ('L Lead Size', 'L ${LeadSize}'),
            ('L Lead Length', 'L ${LeadLength}'),
            ('L Lead Width', 'L ${LeadWidth}'),

            ('Base Body Size X Height', '${BaseBodySize} X ${BodyHeight}'),
            ('Body Length + Diameter', '${BodyLength} + ${BodyDiameter}'),
            ('Body Length X Width X Height', '${BodyLength} X ${BodyWidth} X ${BodyHeight}'),
            ('Body Length + Width X Height', '${BodyLength} + ${BodyWidth} X ${BodyHeight}'),
            ('Body Length X Body Width X Height', '${BodyLength} X ${BodyWidth} X ${BodyHeight}'),
            ('Body Length X Lead Span X Height', '${BodyLength} X ${LeadSpan} X ${BodyHeight}'),
            ('Body Length X Lead Span X Body Height', '${BodyLength} X ${LeadSpan} X ${BodyHeight}'),

            ('Lead Span X Body Height', '${LeadSpan} X ${BodyHeight}'),
            ('Lead Span X Height', '${LeadSpan} X ${BodyHeight}'),
            ('Lead Span X Body Width X Height', '${LeadSpan} X ${BodyWidth} X ${BodyHeight}'),
            ('Lead Span L1 X Lead Span L2 Nominal X Height', '${LeadSpanL1} X ${LeadSpanL2} X ${BodyHeight}'),

            ('T Thermal Pad Length X Width', 'T ${ThermalPadLength} X ${ThermalPadWidth}'),
            ('T Thermal Tab Pad Length X Width', 'T ${ThermalPadLength} X ${ThemalPadWidth}'),

            ('Ball Columns', '${BallColumns}'),
            ('Ball Rows', '${BallRows}'),
            ('Ball Diameter', '${BallDiameter}'),
            ('+ C or N +', '+ ${BallStyle} +'),
            ('Pin Columns', '${PinColumns}'),
            ('Pin Rows', '${PinRows}'),
            ('Col Pitch', '${ColPitch}'),
            ('Row Pitch', '${RowPitch}'),
            ('P Pitch', 'P ${Pitch}'),
            ('Pin Qty.', '${PinQty}'),

            ('+', ''),
            (' ', ''),
        )

        fixups = (
            (u"\u2010", '-'), # Hyphen
            (u"\u2013", '-'), # En Dash
            ('DPAK', 'DPAK (TO)'),
            ('Small Outline Diode', 'Diodes, Small Outline'),
            ('Diodes, Small Outline Flat Lead, 2 Pin', 'Diodes, Small Outline, Flat Lead, 2 Pin'),
            ('Diodes, Small Outline Flat Lead, 3 - 6 Pin', 'Diodes, Small Outline, Flat Lead, 3 - 6 Pin'),
        )

        for input, output in replacements:
            scheme = scheme.replace(input, output)
        for input, output in fixups:
            scheme = scheme.replace(input, output)
            desc = desc.replace(input, output)
        print("%s|%s" % (desc, scheme))
