#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

proj_path = os.getcwd()

# This is so Django knows where to find stuff.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
sys.path.append(proj_path)

# This is so my local_settings.py gets loaded.
os.chdir(proj_path)

# This is so models get loaded.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

import django.core.exceptions

from PyQt5 import QtCore, QtWidgets, QtGui
import ipc7351

class TableModel(QtCore.QAbstractTableModel):
    PkRole = 6666
    def __init__(self, djangoModel, parent = None):
        super(TableModel, self).__init__(parent)
        self.model = djangoModel
        self.filter_kwargs = {}

    def rowCount(self, parentIndex):
        return self.objects().count()

    def columnCount(self, parentIndex):
        return len(self.model._meta.get_fields())

    def data(self, index, role = QtCore.Qt.DisplayRole):
        if not index.isValid():
            return QtCore.QVariant()
        if role == self.PkRole:
            return self.objects()[index.row()].pk
        if role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()
        return self.objects()[index.row()].__getattribute__(self.model._meta.get_fields()[index.column()].name)

    def setFilter(self, **kwargs):
        self.beginResetModel()
        self.filter_kwargs = kwargs
        self.endResetModel()

    def objects(self):
        return self.model.objects.filter(**self.filter_kwargs)


class ListModel(QtCore.QAbstractListModel):
    PkRole = 6666
    def __init__(self, querySet, fieldName):
        super(ListModel, self).__init__()
        self.querySet = querySet
        self.filter_kwargs = {}
        self.fieldName = fieldName

    def rowCount(self, parentIndex):
        return self.objects().count()

    def data(self, index, role = QtCore.Qt.DisplayRole):
        if not index.isValid():
            return QtCore.QVariant()
        if role == self.PkRole:
            return self.objects()[index.row()].pk
        if role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()
        return self.objects()[index.row()].__getattribute__(self.fieldName)

    def setFilter(self, **kwargs):
        self.beginResetModel()
        self.filter_kwargs = kwargs
        self.endResetModel()

    def objects(self):
        return self.querySet.filter(**self.filter_kwargs)

class DimensionTableModel(QtCore.QAbstractTableModel):
    def __init__(self, componentTemplate, componentInstance = None, parent = None):
        super(DimensionTableModel, self).__init__(parent)
        self.componentTemplate = componentTemplate
        self.componentInstance = componentInstance
        self.filter_kwargs = {}

    def headerData(self, section, orientation, role = QtCore.Qt.DisplayRole):
        if self.componentTemplate is None:
            return QtCore.QVariant()
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if section == 0:
                    return "Nominal"
                if section == 1:
                    return "Minimum"
                if section == 2:
                    return "Maximum"
            elif orientation == QtCore.Qt.Vertical:
                return self.componentTemplate.dimensionTemplates.all()[section].dimension
        elif role == QtCore.Qt.DecorationRole:
            if orientation == QtCore.Qt.Vertical:
                if self.componentTemplate.dimensionTemplates.all()[section].isOptional:
                    return QtGui.QIcon.fromTheme("flag-blue")
        return QtCore.QVariant()

    def rowCount(self, parentIndex = QtCore.QModelIndex()):
        if self.componentTemplate is None:
           return 0
        return self.componentTemplate.dimensionTemplates.count()

    def columnCount(self, parentIndex = QtCore.QModelIndex()):
        return 3

    def data(self, index, role = QtCore.Qt.DisplayRole):
        if not index.isValid():
            return QtCore.QVariant()
        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            if self.componentInstance is None:
                return QtCore.QVariant()
            dimInstance = self.componentInstance.dimensionInstances.all()[index.row()]
            dimTemplate = dimInstance.dimensionTemplate
            col = index.column()
            if col == 0:
                return dimInstance.nominalValue
            if col == 1:
                return dimInstance.minimumValue
            if col == 2:
                return dimInstance.maximumValue
        elif role == QtCore.Qt.DecorationRole:
            if self.componentTemplate is None:
                return QtCore.QVariant()
            dimTemplate = self.componentTemplate.dimensionTemplates.all()[index.row()]
            dimInstance = self.componentInstance.dimensionInstances.all()[index.row()]
            col = index.column()
            if col == 0 and dimTemplate.isNominalRequired:
                if len(dimInstance.nominalValue):
                    return QtGui.QIcon.fromTheme("flag-green")
                else:
                    return QtGui.QIcon.fromTheme("flag-red") if not dimTemplate.isOptional else QtGui.QIcon.fromTheme("flag-yellow")
            elif col == 1 and dimTemplate.isMinimumRequired:
                if len(dimInstance.minimumValue):
                    return QtGui.QIcon.fromTheme("flag-green")
                else:
                    return QtGui.QIcon.fromTheme("flag-red") if not dimTemplate.isOptional else QtGui.QIcon.fromTheme("flag-yellow")
            elif col == 2 and dimTemplate.isMaximumRequired:
                if len(dimInstance.maximumValue):
                    return QtGui.QIcon.fromTheme("flag-green")
                else:
                    return QtGui.QIcon.fromTheme("flag-red") if not dimTemplate.isOptional else QtGui.QIcon.fromTheme("flag-yellow")

        return QtCore.QVariant()

    def setData(self, index, value, role = QtCore.Qt.DisplayRole):
        if not index.isValid():
            return QtCore.QVariant()
        if role != QtCore.Qt.EditRole:
            return False
        if self.componentInstance is None:
            return False
        dimInstance = self.componentInstance.dimensionInstances.all()[index.row()]
        col = index.column()
        if col == 0:
            dimInstance.nominalValue = value
        elif col == 1:
            dimInstance.minimumValue = value
        elif col == 2:
            dimInstance.maximumValue = value
        else:
            return False
        dimInstance.save()
        self.dataChanged.emit(index, index)
        return True;

    def flags(self, index):
        return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled

    def setComponentInstance(self, componentInstance):
        self.beginResetModel()
        self.componentInstance = componentInstance
        self.endResetModel()

    def setComponentTemplate(self, componentTemplate):
        self.beginResetModel()
        self.componentTemplate = componentTemplate
        self.endResetModel()


class ComponentTemplateWizardPage(QtWidgets.QWizardPage):
    def __init__(self, parent = None):
        super(ComponentTemplateWizardPage, self).__init__(parent)
        self.setTitle("Template selection");
        self.setSubTitle("Select a template to be used to create the comonent.");
        #self.setPixmap(QtWidgets.QWizard.WatermarkPixmap, QtGui.QPixmap("/home/krys/Pictures/watermark1.png"));

        self.model = ListModel(ipc7351.models.ComponentTemplate.objects, 'name')
        self.view = QtWidgets.QListView()
        self.view.setModel(self.model)

        self.imageWidget = QtWidgets.QLabel()
        self.imageWidget.setBackgroundRole(QtGui.QPalette.Base);
        self.imageWidget.setMinimumSize(64, 64)
        self.imageWidget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding);
        self.imageWidget.setScaledContents(True);

        self.setLayout(QtWidgets.QHBoxLayout())
        self.layout().addWidget(self.view);
        self.layout().setStretchFactor(self.view, 1)
        self.layout().addWidget(self.imageWidget);
        self.layout().setStretchFactor(self.imageWidget, 1)

        self.selected = QtWidgets.QLineEdit()

        def onSelectionChanged(index):
            if index.isValid():
                pk = self.model.data(index, ListModel.PkRole)
                template = ipc7351.models.ComponentTemplate.objects.get(pk=pk)
                reader = QtGui.QImageReader(template.dimensionDrawing.path)
                self.imageWidget.setPixmap(QtGui.QPixmap.fromImage(reader.read()))
                self.selected.setText("%d" % pk)
            else:
                self.selected.setText("")
                self.imageWidget.setPixmap(QtGui.QPixmap())

        self.view.activated.connect(onSelectionChanged)

        self.registerField("componentTemplate*", self.selected)


class ComponentInstanceWizardPage(QtWidgets.QWizardPage):
    def __init__(self, parent = None):
        super(ComponentInstanceWizardPage, self).__init__(parent)

        self.setTitle("Component parameters");
        self.setSubTitle("Specify information about the component you want to create.")
        self.setPixmap(QtWidgets.QWizard.LogoPixmap, QtGui.QPixmap("/home/krys/images/logo1.png"));

        self.instructionLabel = QtWidgets.QLabel("<i>A red flag means that the dimension is required.</i><br/>"
                                                 "<i>A blue flag means that the dimensions in a row are optional.</i><br/>"
                                                 "<i>A yellow flag means that dimension in the blue row is required.</i>");

        self.componentListModel = ListModel(ipc7351.models.Component.objects, 'description')
        self.componentListModel.setFilter(pk=None)
        self.componentListView = QtWidgets.QComboBox()
        self.componentListView.setModel(self.componentListModel)
        self.componentListView.currentIndexChanged.connect(self.setComponent)

        self.componentDimensionModel = DimensionTableModel(None)
        self.componentDimensionView = QtWidgets.QTableView()
        self.componentDimensionView.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed);
        self.componentDimensionView.setModel(self.componentDimensionModel)
        self.componentDimensionView.horizontalHeader().setStretchLastSection(True)
        for i in range(self.componentDimensionView.horizontalHeader().count()):
            self.componentDimensionView.horizontalHeader().setSectionResizeMode(i, QtWidgets.QHeaderView.Stretch);
        self.componentDimensionModel.dataChanged.connect(self.validate)

        self.imageWidget = QtWidgets.QLabel()
        self.imageWidget.setBackgroundRole(QtGui.QPalette.Base);
        self.imageWidget.setMinimumSize(64, 64)
        #self.imageWidget.setMaximumSize(512, 512)
        self.imageWidget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding);
        self.imageWidget.setScaledContents(True);


        layout = QtWidgets.QFormLayout()
        layout.addRow("Family", self.componentListView)
        layout.addRow("Dimension", self.componentDimensionView)
        dataWidget = QtWidgets.QWidget()
        dataWidget.setLayout(layout)
#        self.setLayout(QtWidgets.QVBoxLayout())
#        self.layout().addWidget(dataWidget)

        splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        splitter.setChildrenCollapsible(False)
        splitter.addWidget(dataWidget)
        #splitter.addWidget(self.imageWidget)
        splitter.setStretchFactor(0, 2)
        #splitter.setStretchFactor(0, 1)
        splitter.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding);

        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(splitter)
        self.layout().addStretch()
        self.layout().addWidget(self.instructionLabel)

        self.valid = QtWidgets.QLineEdit()
        self.registerField("valid*", self.valid)

        self.componentTemplate = None
        self.componentInstance = None


    def initializePage(self):
        pk = int(self.field("componentTemplate"))
        self.componentListModel.setFilter(componentTemplates__pk=pk)
        if len(self.componentListModel.objects()) == 0:
            return
        self.componentListView.setCurrentIndex(0)

        self.componentTemplate = ipc7351.models.ComponentTemplate.objects.get(pk=pk)
        self.componentDimensionModel.setComponentTemplate(self.componentTemplate)

        # FIXME: Move children creation logic out of here
        self.componentInstance = ipc7351.models.ComponentInstance(componentTemplate=self.componentTemplate, component=self.componentListModel.objects()[0])
        self.componentInstance.save()
        for dimensionTemplate in self.componentTemplate.dimensionTemplates.all():
            dimensionInstance = ipc7351.models.DimensionInstance(componentInstance=self.componentInstance, dimensionTemplate=dimensionTemplate)
            dimensionInstance.save()
        self.componentDimensionModel.setComponentInstance(self.componentInstance)

        self.imageWidget.setPixmap(QtGui.QPixmap(self.componentTemplate.dimensionDrawing.path))

    def cleanupPage(self):
        if self.componentInstance is not None:
            self.componentInstance.delete()
            self.componentInstance = None
        self.componentTemplate = None

    def validate(self):
        print("Validating")
        # FIXME: Move validation logic into ipc7351.models.ComponentInstance
        try:
            self.componentInstance.clean()
        except django.core.exceptions.ValidationError as errors:
            for field in errors.message_dict.keys():
                print(field, errors.message_dict[field])
                self.valid.setText("")
            return
        self.valid.setText("OK")

    def setComponent(self, index):
        if self.componentInstance is None:
            return
        pk = self.componentListModel.data(self.componentListModel.index(index, 0), ListModel.PkRole)
        self.componentInstance.component = ipc7351.models.Component.objects.get(pk=pk)
        self.componentInstance.save()

class NewComponentInstanceWizard(QtWidgets.QWizard):
    def __init__(self, parent = None):
        super(NewComponentInstanceWizard, self).__init__(parent)
        self.addPage(ComponentTemplateWizardPage())
        self.addPage(ComponentInstanceWizardPage())

        self.setPixmap(QtWidgets.QWizard.BannerPixmap, QtGui.QPixmap("/home/krys/Pictures/banner.png"));
        self.setPixmap(QtWidgets.QWizard.BackgroundPixmap, QtGui.QPixmap("/home/krys/Pictures/background.png"));

        self.setWindowTitle("Component wizard");

    def accept(self):
        super(NewComponentInstanceWizard, self).accept();

    def reject(self):
        super(NewComponentInstanceWizard, self).reject();
        if self.page(1).componentInstance is not None:
            self.page(1).componentInstance.delete()

def componentWizardDemo():
    app = QtWidgets.QApplication([])

    wizard = NewComponentInstanceWizard()
    wizard.show()
    return app.exec_()

componentWizardDemo()
