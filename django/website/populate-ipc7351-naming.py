#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

proj_path = os.getcwd()

# This is so Django knows where to find stuff.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
sys.path.append(proj_path)

# This is so my local_settings.py gets loaded.
os.chdir(proj_path)

# This is so models get loaded.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from ipc7351.models import Component

b = {}
c = {}
with open('ipc7351b-naming.csv') as file:
    for line in file.readlines():
        tokens = line.split('|')
        b[tokens[0]] = tokens[1]


with open('ipc7351c-naming.csv') as file:
    for line in file.readlines():
        tokens = line.split('|')
        c[tokens[0]] = tokens[1]

for entry in list(set(b.keys())|set(c.keys())):
    bname = ""
    cname = ""
    if entry in b:
        bname = b[entry]
    if entry in c:
        cname = c[entry]
    comp = Component(description=entry, namingRevisionB=bname, namingRevisionC=cname)
    comp.save()
