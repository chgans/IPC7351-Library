#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

proj_path = os.getcwd()

# This is so Django knows where to find stuff.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
sys.path.append(proj_path)

# This is so my local_settings.py gets loaded.
os.chdir(proj_path)

# This is so models get loaded.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
import re

from ipc7351.models import Manufacturer

with open('library-expert-manufacturer-naming-ipc7351c-nov-2016.txt') as file:
    for line in file.readlines():
        line = re.sub(r'\.\.+', '|', line)
        tokens=line.split('|')
        manufacturer, created = Manufacturer.objects.get_or_create(code=tokens[1].strip(), name=tokens[0].strip())
        if created:
            manufacturer.save()
