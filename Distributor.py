# -*- coding: utf-8 -*-

import math

from PySide import QtGui, QtCore

import FreeCAD as App

class Distributor:

    def __init__(self, rowCount, rowPitch, rowSpan, colCount = 0, colPitch = 0, colSpan = 0):

        self.rowCount = int(rowCount)
        self.rowPitch = float(rowPitch)
        self.rowSpan = float(rowSpan)

        self.colCount = int(colCount)
        self.colPitch = float(colPitch)
        self.colSpan = float(colSpan)

        self._left = []
        self._right = []
        self._bot = []
        self._top = []
        self._calculate()

    # "Diagonal-cross" distribution, aka distribute on 4 sides
    # TODO: "straight-cross" distribution, aka distribute on 4 corners
    def _calculate(self):

        rowX = -self.rowSpan/2.0
        rowY = (int(self.rowCount/2) - ((self.rowCount+1)%2)/2.0) * self.rowPitch

        for r in range(self.rowCount):
            y = rowY - r*self.rowPitch
            m = App.Matrix()
            m.rotateZ(math.pi)
            m.move((rowX, y, 0))
            self._left.append(m)
            m = App.Matrix()
            m.rotateZ(0)
            m.move((-rowX, -y, 0))
            self._right.append(m)

        colX = -(int(self.colCount/2) - ((self.colCount+1)%2)/2.0) * self.colPitch
        colY = -self.colSpan/2.0

        for c in range(self.colCount):
            x = colX + c*self.colPitch
            m = App.Matrix()
            m.rotateZ(-math.pi/2)
            m.move((x, colY, 0))
            self._bot.append(m)
            m = App.Matrix()
            m.rotateZ(math.pi/2)
            m.move((-x, -colY, 0))
            self._top.append(m)

    def distribution(self):
        return (self._bot, self._right, self._top, self._left)

