Ball Grid Array’s;BGA;${PinCount}${CollpasingBalls}${Pitch}P${BallColumnCount}X${BallRowCount}_${BodyLength}X${BodyWidth}X${Height}
BGA w/Dual Pitch;BGA;${PinCount}${CollpasingBalls}${ColPitch}X${RowPitch}P${BallColumnCount}X${BallRowCount}_${BodyLength}X${BodyWidth}X${Height}
BGA w/Staggered Pins;BGAS;${PinCount}${CollpasingBalls}${Pitch} P${BallColumnCount}X${BallRowCount}_${BodyLength}X${BodyWidth}X${Height}
Capacitors;Chip, Array, Concave;CAPCAV;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Capacitors;Chip, Array, Flat;CAPCAF;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Capacitors;Chip;CAPC;${BodyLength}${BodyWidth}X${Height}
Capacitors;Polarized, Chip;CAPPC;${BodyLength}${BodyWidth}X${Height}
Capacitors;Dual Flat No‐lead;CAPDFN;${BodyLength}${BodyWidth}X${Height}
Capacitors;Polarized, Dual Flat No‐lead;CAPPDFN;${BodyLength}${BodyWidth}X${Height}
Capacitors;Molded;CAPM;${BodyLength}${BodyWidth}X${Height}
Capacitors;Polarized, Molded;CAPPM;${BodyLength}${BodyWidth}X${Height}
Capacitors;Aluminum Electrolytic;CAPAE;${BaseBodySize}X${Height}
Ceramic Flat Packages;CFP127P;${LeadSpanNominal}X${Height}-${PinCount}
Column Grid Array;Circular Lead;CGA;${PinCount}C${Pitch}P${PinColumnCount}X${PinRowCount}_${BodyLength}X${BodyWidth}X${Height}
Pillar Column Grid Array;PCGA;${PinCount}S${Pitch}P${PinColumnCount}X${PinRowCount}_${BodyLength}X${BodyWidth}X${Height}
Crystals (2 leads);XTAL;${BodyLength}X${BodyWidth}X${Height}
Crystals;Dual Flat No‐lead;XTALDFN;${BodyLength}X${BodyWidth}X${Height}
Crystals;Side Concave;XTALSC;${BodyLength}X${BodyWidth}X${Height}
Diodes;Chip;DIOC;${BodyLength}${BodyWidth}X${Height}
Diodes;Dual Flat No‐lead;DIODFN;${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Diodes;Molded;DIOM;${BodyLength}${BodyWidth}X${Height}
Diodes;Non‐polarized Chip;DIONC;${BodyLength}${BodyWidth}X${Height}
Diodes;Non‐polarized Molded;DIONM;${BodyLength}${BodyWidth}X${Height}
Diodes;MELF;DIOMELF;${BodyLength}${BodyDiameter}
Diodes;Side Concave, 2 Pin;DIOSC;${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Diodes;Side Concave, 4 Pin, DIOSC+ ${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Ferrite Bead;Chip;BEADC;${BodyLength}${BodyWidth}X${Height}
Fuses;Chip;FUSC;${BodyLength}${BodyWidth}X${Height}
Fuses;Dual Flat No‐Lead;FUSDFN;${BodyLength}${BodyWidth}X${Height}
Fuses;Molded;FUSM;${BodyLength}${BodyWidth}X${Height}
Fuses;Side Concave;FUSSC;${BodyLength}${BodyWidth}X${Height}
Inductors;Chip;INDC;${BodyLength}${BodyWidth}X${Height}
Inductors;Chip, Array, Concave;INDCAV;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Inductors;Chip, Array, Flat;INDCAF;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Inductors;Dual Flat No‐lead;INDDFN;${BodyLength}${BodyWidth}X${Height}
Inductors;Molded;INDM;${BodyLength}${BodyWidth}X${Height}
Inductors;Precision, Molded;INDPM;${BodyLength}${BodyWidth}X${Height}
Inductors;Side Concave;INDSC;${BodyLength}${BodyWidth}X${Height}
Land Grid Array;Circular Lead;LGA;${PinCount}C${Pitch}P${PinColumnCount}X${PinRowCount}_${BodyLength}X${BodyWidth}X${Height}
Land Grid Array;Square Lead;LGA;${PinCount}S${Pitch}P${PinColumnCount}X${PinRowCount}_${BodyLength}X${BodyWidth}X${Height}
LED’s;Chip;LEDC;${BodyLength}${BodyWidth}X${Height}
LED’s;Dual Flat No‐lead;LEDDFN;${BodyLength}${BodyWidth}X${Height}
LED’s;Molded;LEDM;${BodyLength}${BodyWidth}X${Height}
LED’s;Side Concave, 2 Pin;LEDSC;${BodyLength}X${BodyWidth}X${Height}-${PinCount}
LED’s;Side Concave, 4 Pin;LEDSC;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Oscillators;Dual Flat No‐lead;OSCDFN;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Oscillators;Side Concave;OSCSC;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Oscillators;Side Flat;OSCSF;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Oscillators;J‐Lead;OSCJ;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Oscillators;L‐Bend Lead;OSCL;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Oscillators;Corner Concave;OSCCC;${BodyLength}X${BodyWidth}X${Height}
Plastic Leaded Chip Carriers;PLCC;${Pitch}P${LeadSpanL1}X${LeadSpanL2Nominal}X${Height}-${PinCount}
Plastic Leaded Chip Carrier Sockets Square;PLCCS;${Pitch}P${LeadSpanL1}X${LeadSpanL2Nominal}X${Height}-${PinCount}
Quad Flat Packages;QFP;${Pitch}P${LeadSpanL1}X${LeadSpanL2Nominal}X${Height}-${PinCount}
Ceramic Quad Flat Packages;CQFP;${Pitch}P${LeadSpanL1}X${LeadSpanL2Nominal}X${Height}-${PinCount}
Quad Flat No‐lead;QFN;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}${ThermalPad}
Pull‐back Quad Flat No‐lead;PQFN;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}${ThermalPad}
Quad Leadless Ceramic Chip Carriers;LCC;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
© 2016 PCB Libraries;Inc,
Quad Leadless Ceramic Chip Carriers (Pin 1 on Side);LCCS;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Resistors;Chip;RESC;${BodyLength}${BodyWidth}X${Height}
Resistors;Chip, Array, Concave;RESCAV;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Resistors;Chip, Array, Convex, E‐Version (Even Pin Size);RESCAXE;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Resistors;Chip, Array, Convex, S‐Version (Side Pins Diff);RESCAXS;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Resistors;Chip, Array, Flat;RESCAF;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Resistors;Dual Flat No‐lead;RESDFN;${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Resistors;MELF;RESMELF;${BodyLength}${BodyDiameter}
Resistors;Molded;RESM;${BodyLength}${BodyWidth}X${Height}
Resistors;Side Concave;RESSC;${BodyLength}${BodyWidth}X${Height}
Small Outline Diodes;Flat Lead;SODFL;${LeadSpanNominal}${BodyWidth}X${Height}
Small Outline IC;J‐Leaded;SOJ;${Pitch}P${LeadSpanNominal}X${Height}-${PinCount}
Small Outline IC;L‐Leaded;SOL;${Pitch}P${LeadSpanNominal}X${Height}-${PinCount}
Small Outline Integrated Circuit;(50 mil ${Pitch} SOIC);SOIC127P;${LeadSpanNominal}X${Height}-${PinCount}
Small Outline Packages;SOP;${Pitch}P${LeadSpanNominal}X${Height}-${PinCount}
Small Outline No‐lead;SON;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}${ThermalPad}
Thermistors;Chip;THRMC;${BodyLength}${BodyWidth}X${Height}
Pull‐back Small Outline No‐lead;PSON;${Pitch}P${BodyLength}X${BodyWidth}X${Height}-${PinCount}${ThermalPad}
Small Outline Transistors;Flat Lead;SOTFL;${Pitch}P${LeadSpanNominal}X${Height}-${PinCount}
SOD (Example: SOD3717X135 = JEDEC SOD123);SOD;${LeadSpanNominal}${BodyWidth}X${Height}
SOT143 & SOT343 (JEDEC Standard Package);SOT;${Pitch}P${LeadSpanNominal}X${Height}-${PinCount}
SOT143 & SOT343 Reverse (JEDEC Standard Package);SOT;${Pitch}P${LeadSpanNominal}X${Height}-${PinCount}R
SOT23 & SOT223 Packages (Example: SOT230P700X180‐4);SOT;${Pitch}P${LeadSpanNominal}X${Height}-${PinCount}
TO (Generic DPAK-Example: TO228P970X238‐3);TO;${Pitch}P${LeadSpan}X${Height}-${PinCount}
Transistors;Dual Flat No‐lead;TRXDFN;${BodyLength}X${BodyWidth}X${Height}-${PinCount}
Varistors;Chip;VARC;${BodyLength}${BodyWidth}X${Height}
