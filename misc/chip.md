Capacitor|Chip|Rectangular End Cap|CAPC
Capacitor|Polarised Chip|Rectangular End Cap|CAPPC
Diode|Chip|Rectangular End Cap|DIOC
Diode|Non Polarised Chip|Rectangular End Cap|DIONC
Ferrite Bead|Chip|Rectangular End Cap|BEADC
Fuse|Chip|Rectangular End Cap|FUSC
Inductor|Chip|Rectangular End Cap|INDC
LED|Chip|Rectangular End Cap|LEDC
Resistor|Chip|Rectangular End Cap|RESC
Thermistor|Chip|Rectangular End Cap|THRMC
Varistor|Chip|Rectangular End Cap|VARC
